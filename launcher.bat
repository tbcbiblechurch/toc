@echo off

echo [0m
echo [0m
echo [36m ______   ______     ______    
echo /\__  _\ /\  __ \   /\  ___\   
echo \/_/\ \/ \ \ \/\ \  \ \ \____  
echo    \ \_\  \ \_____\  \ \_____\ 
echo     \/_/   \/_____/   \/_____/ 
echo [0m
echo [0m
echo TBC OBS Controller
echo Developed and maintained by TBC Bible Church
echo gitlab.com/tbcbiblechurch

cd %~dp0
start powershell.exe -NoExit -executionpolicy remotesigned -windowstyle hidden -file .\tocform.ps1"
echo [0m
echo [0m
echo Launching...
timeout /T 4 > NUL