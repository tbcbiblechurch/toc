#Global Variables

$SyncHash = [hashtable]::Synchronized(@{})

$Global:SelectedBookName = "Genesis"
$Global:ReferenceIsLocked = $false
$Global:ReferenceSource = "overlay"

. .\books.ps1

function SaveConfig {
    $Global:Config.Save(".\config.xml")
}

function SaveAll {
    SaveConfig
    SaveChecklists
}

function LoadConfig {
    [XML]$Global:Config = Get-Content .\config.xml
    $dropOBSScene.Text = $Global:Config.configuration.obs.selectedScene
    $dropOBSSource.Text = $Global:Config.configuration.obs.selectedSource
}

function OBSInitialize {
    Add-Type -path ‘resources\obs-websocket-dotnet.dll’
    $Global:OBS = new-object OBSWebsocketDotNet.OBSWebsocket
}

function OBSConnect {
    $obsprocess = Get-Process obs64 -ErrorAction SilentlyContinue
    if ($obsprocess){
    if ( -not ($Global:OBS.IsConnected)){
        $Global:OBS.Connect("ws://127.0.0.1:4444", "")
    }
    if ($Global:OBS.IsConnected){
        $statusBar.Text = "OBS Connected"
        $pnlOBS.BackColor = [System.Drawing.Color]::FromArgb(255,0,255,0)
        $lblOBSStatus.Text = "Connected"
    } else {
        $statusBar.Text = "OBS Offline"
        $pnlOBS.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)
        $lblOBSStatus.Text = "Not Connected"
    }
    }
}

function OBSCheckConnection{
    if ( -not ($Global:OBS.IsConnected)){
        OBSConnect
        if ( -not ($Global:OBS.IsConnected)){
            return $false
        }
    }
    if ($Global:OBS.IsConnected) {
        return $true
    }
}

function ShowReference{
    if(OBSCheckConnection){
        #save current items to restore later
        $currentLiveScene = $Global:OBS.GetCurrentScene()
         $currentLiveSceneName = $currentLiveScene.Name
        $currentPreviewScene = $Global:OBS.GetPreviewScene()
         $currentPreviewSceneName = $currentPreviewScene.Name
        $currentTransition = $Global:OBS.GetCurrentTransition()
         $currentTransitionName = $currentTransition.Name
         start-sleep 1

        #show the reference
        $Global:OBS.SetPreviewScene("$currentLiveSceneName")
        $Global:OBS.SetSourceRender("$Global:ReferenceSource", $true, "$currentLiveSceneName")
        $Global:OBS.SetCurrentTransition("ref-in")
        $Global:OBS.TransitionToProgram()
        start-sleep 1

        #set user items back
        $Global:OBS.SetPreviewScene("$currentPreviewSceneName")
        $Global:OBS.SetCurrentTransition("$currentTransitionName")
    }
}


function HideReference{
    if(OBSCheckConnection){
        #save current items to restore later
        $currentLiveScene = $Global:OBS.GetCurrentScene()
         $currentLiveSceneName = $currentLiveScene.Name
        $currentPreviewScene = $Global:OBS.GetPreviewScene()
         $currentPreviewSceneName = $currentPreviewScene.Name
        $currentTransition = $Global:OBS.GetCurrentTransition()
         $currentTransitionName = $currentTransition.Name
         start-sleep 1

        #show the reference
        $Global:OBS.SetPreviewScene("$currentLiveSceneName")
        $Global:OBS.SetSourceRender("$Global:ReferenceSource", $false, "$currentLiveSceneName")
        $Global:OBS.SetCurrentTransition("ref-out")
        $Global:OBS.TransitionToProgram()
        start-sleep 1

        #set user items back
        $Global:OBS.SetPreviewScene("$currentPreviewSceneName")
        $Global:OBS.SetCurrentTransition("$currentTransitionName")
    }
}

function LoadOBSSources {
    if(OBSCheckConnection){
        $Global:obsScenes = $Global:OBS.ListScenes()
        $dropOBSScene.Items.Clear()
        foreach ($item in $Global:obsScenes){
            $dropOBSScene.Items.Add($item.Name)
        }
    }
}

function UpdateSources {
    $dropOBSSource.Items.Clear()
    $i = 0
    foreach ($item in $dropOBSScene.Items){
        if ($item -eq $dropOBSScene.Text){
            foreach ($source in $Global:obsScenes[$i].Items){
                $dropOBSSource.Items.Add($source.SourceName)
            }
        }
        $i++
    }
}

function SetReferenceSource {
    $Global:ReferenceSource = $dropOBSSource.Text
    $Global:Config.configuration.obs.selectedScene = $dropOBSScene.Text
    $Global:Config.configuration.obs.selectedSource = $dropOBSSource.Text
    SaveConfig
}

function LoadTitles {
$titles = Get-Content -Path .\titles.txt
$listTitles.Items.Clear()
foreach($title in $titles){
    $listTitles.Items.add($title) | Out-Null
    $txtTitles.AppendText("$title`n")
}
$listTitles.SelectedIndex = 0
Set-Content -Encoding UTF8 -Path .\currenttitle.txt -Value $listTitles.SelectedItem
}

function startCountdown {
    # Created the Powershell instance that will run my script block.
    $instanceCountdown = [PowerShell]::Create().AddScript({
        # Countdown timer for TOC
        
        $hour = $SyncHash.nHour.Text
        $minute = $SyncHash.nMinute.Text
        if ($SyncHash.nAm.SelectedItem -eq "pm") {
            $hour = [int]$hour + 12
        }
        #$hour = 12
        #$minute = 30
        
        Set-Content -Encoding UTF8 -Path .\log.txt -Value "Hour:$hour Minute:$minute"
        $now = (Get-Date)
        
        $targetYear=$now.Year
        $targetMonth=$now.Month
        $targetDay=$now.Day
        
        [datetime]$countdownto = "$targetMonth/$targetDay/$targetYear ${hour}:$minute"
        Set-Content -Encoding UTF8 -Path .\countdownlock -Value ""
        
        
        do {
        if (-not (Test-Path -Path .\countdownlock)){
            break
        }
        $now = (Get-Date)
        
        $difference = ($countdownto-$now)
        $h = $difference.Hours
        $m = $difference.Minutes
        $s = $difference.Seconds
        $timeleft = "${h}:${m}:${s}"
        $timeleft | Out-File .\countdown.txt
        $syncHash.lblCountdown.text = $timeleft
        start-sleep -seconds 1
        } until ((Get-Date) -gt $countdownto)
    })
    # Created a new runspace
    $RunspaceCountdown = [RunspaceFactory]::CreateRunspace()
    $RunspaceCountdown.ApartmentState = "STA"
    # The command below only allowed 1 click to be executed at a time, even if you clicked the hell out of the button while it was in progress. I commented it out for the ability to have more than 1 thread created in this runspace.
    # $NewUserRunspace.ThreadOptions = "ReuseThread"
    $RunspaceCountdown.Open()
    $RunspaceCountdown.SessionStateProxy.SetVariable("SyncHash",$SyncHash)
    # Assigned my new Powershell Instance to the runspace I just created.
    $instanceCountdown.Runspace = $RunspaceCountdown
    # Invoked the Powershell Instance/Script.
    $instanceCountdown.BeginInvoke()

}


function stopCountdown {
    Remove-Item .\countdownlock
}

function selectNextTitle {
    if ($listTitles.SelectedIndex -lt ($listTitles.Items.Count - 1)){
        $listTitles.SelectedIndex = $listTitles.SelectedIndex + 1
        Set-Content -Encoding UTF8 -Path .\currenttitle.txt -Value $listTitles.SelectedItem
    }
}

function selectPreviousTitle {
    if ($listTitles.SelectedIndex -gt 0){
        $listTitles.SelectedIndex = $listTitles.SelectedIndex - 1
        Set-Content -Encoding UTF8 -Path .\currenttitle.txt -Value $listTitles.SelectedItem
    }
}

function toggleTitleEditor {
    if ($txtTitles.Visible -eq $false){
        #Turn on edit mode
        $listTitles.Visible = $false
        $txtTitles.Visible = $true
        $btnTitlesEdit.Text = "Save"
    }else{
        #Save and return to view
        $listTitles.Items.Clear()
        Remove-Item .\titles.txt
        foreach ($line in $txtTitles.Lines){
            if ($line -ne "") {
                $listTitles.Items.add("$line") | Out-Null
                Add-Content .\titles.txt "$line"
            }
        }
        $txtTitles.Text = ""
        foreach ($item in $listTitles.Items){
            if ($txtTitles.Text -eq "") {
                $txtTitles.Text = $item
            } else {
                $txtTitles.Text = $txtTitles.Text + "`r`n$item"
            }
        }

        $txtTitles.Visible = $false
        $listTitles.Visible = $true
        $btnTitlesEdit.Text = "Edit"
        $listTitles.SelectedIndex = 0
    }
}

function SetReference{
    Param (
        $book,
        $chapter,
        $verse
    )
    $compiledReference = "$book ${chapter}:$verse"
    $Global:SelectedBookName = $book
    $lblRefSelected.text = $compiledReference
    if(-not ($Global:ReferenceIsLocked)) {
        Set-Content -Encoding UTF8 -Path .\reference.txt -Value $compiledReference
        $lblRefLive.text = $compiledReference
    }
}

function ToggleReferenceLock {
    if ($Global:ReferenceIsLocked){
        $Global:ReferenceIsLocked = $false
        $btnRefLock.Text = "Lock"
        UpdateReference
        $btnRefLock.BackColor = [System.Drawing.Color]::FromArgb(255,255,99,71)
    }else{
        $Global:ReferenceIsLocked = $true
        $btnRefLock.Text = "apply"
        $btnRefLock.BackColor = [System.Drawing.Color]::FromArgb(255,144,238,144)
    }    
}

function GetRefBook($book) {
    $selectedBook = $BookNames.$book
    SetReference $selectedBook $nChapter.text $nVerse.text
}

function UpdateReference{
    SetReference $SelectedBookName $nChapter.text $nVerse.text
}

function SetTitle {
    Set-Content -Encoding UTF8 -Path .\currenttitle.txt -Value $listTitles.SelectedItem
}


function LoadChecklists {
$configitems = Get-Content -Path .\checklists.txt

foreach($item in $configitems){
    $configuration = $item.Split("=")
    $var = $(Get-Variable -Name $configuration[0])
    $(Get-Variable -Name $configuration[0]).Value.Text = $configuration[1]
    $checkbox = "c"
    if ($configuration[0] -like "*Prep*"){
        $number = $configuration[0].TrimStart("txtConfPrep")
        $checkbox = "${checkbox}p$number"
    }else {
    if ($configuration[0] -like "*Stream*"){
        $number = $configuration[0].TrimStart("txtConfStream")
        $checkbox = "${checkbox}s$number"
    
    }else{
    if ($configuration[0] -like "*Audio*"){
        $number = $configuration[0].TrimStart("txtConfAudio")
        $checkbox = "${checkbox}a$number"
    }
    }
    }
    if($configuration[1] -ne ""){
        $(Get-Variable -Name $checkbox).Value.Text = $configuration[1]
        $(Get-Variable -Name $checkbox).Value.Visible = $true
    } else {
        $(Get-Variable -Name $checkbox).Value.Visible = $false
    }
}
    
}

function SaveChecklists {
    Remove-Item .\checklists.txt
    Add-Content .\checklists.txt ("txtConfPrep1=" + $txtConfPrep1.text)
    Add-Content .\checklists.txt ("txtConfPrep2=" + $txtConfPrep2.text)
    Add-Content .\checklists.txt ("txtConfPrep3=" + $txtConfPrep3.text)
    Add-Content .\checklists.txt ("txtConfPrep4=" + $txtConfPrep4.text)
    Add-Content .\checklists.txt ("txtConfPrep5=" + $txtConfPrep5.text)
    Add-Content .\checklists.txt ("txtConfPrep6=" + $txtConfPrep6.text)
    Add-Content .\checklists.txt ("txtConfPrep7=" + $txtConfPrep7.text)
    Add-Content .\checklists.txt ("txtConfPrep8=" + $txtConfPrep8.text)
    Add-Content .\checklists.txt ("txtConfPrep9=" + $txtConfPrep9.text)
    Add-Content .\checklists.txt ("txtConfPrep10=" + $txtConfPrep10.text)
    Add-Content .\checklists.txt ("txtConfStream1=" + $txtConfStream1.text)
    Add-Content .\checklists.txt ("txtConfStream2=" + $txtConfStream2.text)
    Add-Content .\checklists.txt ("txtConfStream3=" + $txtConfStream3.text)
    Add-Content .\checklists.txt ("txtConfStream4=" + $txtConfStream4.text)
    Add-Content .\checklists.txt ("txtConfStream5=" + $txtConfStream5.text)
    Add-Content .\checklists.txt ("txtConfStream6=" + $txtConfStream6.text)
    Add-Content .\checklists.txt ("txtConfStream7=" + $txtConfStream7.text)
    Add-Content .\checklists.txt ("txtConfStream8=" + $txtConfStream8.text)
    Add-Content .\checklists.txt ("txtConfStream9=" + $txtConfStream9.text)
    Add-Content .\checklists.txt ("txtConfStream10=" + $txtConfStream10.text)
    Add-Content .\checklists.txt ("txtConfAudio1=" + $txtConfAudio1.text)
    Add-Content .\checklists.txt ("txtConfAudio2=" + $txtConfAudio2.text)
    Add-Content .\checklists.txt ("txtConfAudio3=" + $txtConfAudio3.text)
    Add-Content .\checklists.txt ("txtConfAudio4=" + $txtConfAudio4.text)
    Add-Content .\checklists.txt ("txtConfAudio5=" + $txtConfAudio5.text)
    Add-Content .\checklists.txt ("txtConfAudio6=" + $txtConfAudio6.text)
    Add-Content .\checklists.txt ("txtConfAudio7=" + $txtConfAudio7.text)
    Add-Content .\checklists.txt ("txtConfAudio8=" + $txtConfAudio8.text)
    Add-Content .\checklists.txt ("txtConfAudio9=" + $txtConfAudio9.text)
    Add-Content .\checklists.txt ("txtConfAudio10=" + $txtConfAudio10.text)
    
    LoadChecklists
    $tabMain.SelectedTab = $tabMainChecklists

}

function checkAllLists {
    if (($pnlPrep.BackColor -eq [System.Drawing.Color]::FromArgb(255,0,255,0)) -and `
        ($pnlStream.BackColor -eq [System.Drawing.Color]::FromArgb(255,0,255,0)) -and `
        ($pnlAudio.BackColor -eq [System.Drawing.Color]::FromArgb(255,0,255,0))){
            $pnlReady.BackColor = [System.Drawing.Color]::FromArgb(255,0,255,0)   
    } else {
            $pnlReady.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)  
    }
}

function checkPrepList {
    $allChecked = $false
    For ($i=1; $i -le 10; $i++){
        if ($(Get-Variable -Name "cp$i").Value.Visible -eq $true){
            if ($(Get-Variable -Name "cp$i").Value.Checked -eq $false){
                $pnlPrep.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)
                checkAllLists
                return
            }
        }    
    }
    $pnlPrep.BackColor = [System.Drawing.Color]::FromArgb(255,0,255,0)
    checkAllLists
}

function checkStreamList {
    $allChecked = $false
    For ($i=1; $i -le 10; $i++){
        if ($(Get-Variable -Name "cs$i").Value.Visible -eq $true){
            if ($(Get-Variable -Name "cs$i").Value.Checked -eq $false){
                $pnlStream.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)
                checkAllLists
                return
            }
        }    
    }
    $pnlStream.BackColor = [System.Drawing.Color]::FromArgb(255,0,255,0)
    checkAllLists
}

function checkAudioList {
    $allChecked = $false
    For ($i=1; $i -le 10; $i++){
        if ($(Get-Variable -Name "ca$i").Value.Visible -eq $true){
            if ($(Get-Variable -Name "ca$i").Value.Checked -eq $false){
                $pnlAudio.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)
                checkAllLists
                return
            }
        }    
    }
    $pnlAudio.BackColor = [System.Drawing.Color]::FromArgb(255,0,255,0)
    checkAllLists
}



#Generated Form Function
function showTOC {
########################################################################
# Code Generated By: SAPIEN Technologies PrimalForms (Community Edition) v1.0.10.0
# Generated On: 4/28/2020 7:18 PM
# Generated By: admin
########################################################################

#region Import the Assemblies
[reflection.assembly]::loadwithpartialname("System.Windows.Forms") | Out-Null
[reflection.assembly]::loadwithpartialname("System.Drawing") | Out-Null
#endregion

#region Generated Form Objects
$formTOC = New-Object System.Windows.Forms.Form
$statusBar = New-Object System.Windows.Forms.StatusBar
$lblPrep = New-Object System.Windows.Forms.Label
$pnlPrep = New-Object System.Windows.Forms.Panel
$lblStream = New-Object System.Windows.Forms.Label
$pnlStream = New-Object System.Windows.Forms.Panel
$lblAudio = New-Object System.Windows.Forms.Label
$lblAllReady = New-Object System.Windows.Forms.Label
$pnlAudio = New-Object System.Windows.Forms.Panel
$pnlReady = New-Object System.Windows.Forms.Panel
$tabMain = New-Object System.Windows.Forms.TabControl
$tabMainControls = New-Object System.Windows.Forms.TabPage
$grpRef = New-Object System.Windows.Forms.GroupBox
$nVerse = New-Object System.Windows.Forms.NumericUpDown
$lblVerse = New-Object System.Windows.Forms.Label
$lblChapter = New-Object System.Windows.Forms.Label
$nChapter = New-Object System.Windows.Forms.NumericUpDown
$bRev = New-Object System.Windows.Forms.Button
$bJud = New-Object System.Windows.Forms.Button
$b3jn = New-Object System.Windows.Forms.Button
$b2jn = New-Object System.Windows.Forms.Button
$b1jn = New-Object System.Windows.Forms.Button
$b2pe = New-Object System.Windows.Forms.Button
$b1pe = New-Object System.Windows.Forms.Button
$bJas = New-Object System.Windows.Forms.Button
$bHeb = New-Object System.Windows.Forms.Button
$bPhm = New-Object System.Windows.Forms.Button
$bTts = New-Object System.Windows.Forms.Button
$b2ti = New-Object System.Windows.Forms.Button
$b1ti = New-Object System.Windows.Forms.Button
$b2th = New-Object System.Windows.Forms.Button
$b1th = New-Object System.Windows.Forms.Button
$bCol = New-Object System.Windows.Forms.Button
$bPhp = New-Object System.Windows.Forms.Button
$bEph = New-Object System.Windows.Forms.Button
$bGal = New-Object System.Windows.Forms.Button
$b2co = New-Object System.Windows.Forms.Button
$b1co = New-Object System.Windows.Forms.Button
$bRom = New-Object System.Windows.Forms.Button
$bAct = New-Object System.Windows.Forms.Button
$bJhn = New-Object System.Windows.Forms.Button
$bLuk = New-Object System.Windows.Forms.Button
$bMar = New-Object System.Windows.Forms.Button
$bMtt = New-Object System.Windows.Forms.Button
$bMal = New-Object System.Windows.Forms.Button
$bZec = New-Object System.Windows.Forms.Button
$bHgg = New-Object System.Windows.Forms.Button
$bZep = New-Object System.Windows.Forms.Button
$bHab = New-Object System.Windows.Forms.Button
$bNah = New-Object System.Windows.Forms.Button
$bMic = New-Object System.Windows.Forms.Button
$bJnh = New-Object System.Windows.Forms.Button
$bOba = New-Object System.Windows.Forms.Button
$bAmo = New-Object System.Windows.Forms.Button
$bJoe = New-Object System.Windows.Forms.Button
$bHos = New-Object System.Windows.Forms.Button
$bDan = New-Object System.Windows.Forms.Button
$bEze = New-Object System.Windows.Forms.Button
$bLam = New-Object System.Windows.Forms.Button
$bJer = New-Object System.Windows.Forms.Button
$bIsa = New-Object System.Windows.Forms.Button
$bSng = New-Object System.Windows.Forms.Button
$bEcc = New-Object System.Windows.Forms.Button
$bPro = New-Object System.Windows.Forms.Button
$bPsa = New-Object System.Windows.Forms.Button
$bJob = New-Object System.Windows.Forms.Button
$bEst = New-Object System.Windows.Forms.Button
$bNeh = New-Object System.Windows.Forms.Button
$bEzr = New-Object System.Windows.Forms.Button
$b2ch = New-Object System.Windows.Forms.Button
$b1ch = New-Object System.Windows.Forms.Button
$b2Ki = New-Object System.Windows.Forms.Button
$b1ki = New-Object System.Windows.Forms.Button
$b2sa = New-Object System.Windows.Forms.Button
$b1sa = New-Object System.Windows.Forms.Button
$bRth = New-Object System.Windows.Forms.Button
$bJdg = New-Object System.Windows.Forms.Button
$bJos = New-Object System.Windows.Forms.Button
$bDeu = New-Object System.Windows.Forms.Button
$bNum = New-Object System.Windows.Forms.Button
$bLev = New-Object System.Windows.Forms.Button
$bExo = New-Object System.Windows.Forms.Button
$bGen = New-Object System.Windows.Forms.Button
$lblRefLive = New-Object System.Windows.Forms.Label
$lblLiveReferenceLabel = New-Object System.Windows.Forms.Label
$btnRefLock = New-Object System.Windows.Forms.Button
$btnRefHide = New-Object System.Windows.Forms.Button
$btnRefShow = New-Object System.Windows.Forms.Button
$lblRefSelected = New-Object System.Windows.Forms.Label
$lblSelectedReferenceLabel = New-Object System.Windows.Forms.Label
$txtSearch = New-Object System.Windows.Forms.TextBox
$grpTitles = New-Object System.Windows.Forms.GroupBox
$btnTitlesEdit = New-Object System.Windows.Forms.Button
$btnTitleNext = New-Object System.Windows.Forms.Button
$btnTitlePrevious = New-Object System.Windows.Forms.Button
$listTitles = New-Object System.Windows.Forms.ListBox
$txtTitles = New-Object System.Windows.Forms.TextBox
$grpCountdown = New-Object System.Windows.Forms.GroupBox
$btnCountdownStop = New-Object System.Windows.Forms.Button
$btnCountdownStart = New-Object System.Windows.Forms.Button
$tabMainChecklists = New-Object System.Windows.Forms.TabPage
$grpAudio = New-Object System.Windows.Forms.GroupBox
$ca10 = New-Object System.Windows.Forms.CheckBox
$ca9 = New-Object System.Windows.Forms.CheckBox
$ca8 = New-Object System.Windows.Forms.CheckBox
$ca7 = New-Object System.Windows.Forms.CheckBox
$ca6 = New-Object System.Windows.Forms.CheckBox
$ca5 = New-Object System.Windows.Forms.CheckBox
$ca4 = New-Object System.Windows.Forms.CheckBox
$ca3 = New-Object System.Windows.Forms.CheckBox
$ca2 = New-Object System.Windows.Forms.CheckBox
$ca1 = New-Object System.Windows.Forms.CheckBox
$grpStream = New-Object System.Windows.Forms.GroupBox
$cs9 = New-Object System.Windows.Forms.CheckBox
$cs10 = New-Object System.Windows.Forms.CheckBox
$cs8 = New-Object System.Windows.Forms.CheckBox
$cs7 = New-Object System.Windows.Forms.CheckBox
$cs6 = New-Object System.Windows.Forms.CheckBox
$cs5 = New-Object System.Windows.Forms.CheckBox
$cs4 = New-Object System.Windows.Forms.CheckBox
$cs3 = New-Object System.Windows.Forms.CheckBox
$cs2 = New-Object System.Windows.Forms.CheckBox
$cs1 = New-Object System.Windows.Forms.CheckBox
$grpPreparation = New-Object System.Windows.Forms.GroupBox
$cp10 = New-Object System.Windows.Forms.CheckBox
$cp9 = New-Object System.Windows.Forms.CheckBox
$cp8 = New-Object System.Windows.Forms.CheckBox
$cp7 = New-Object System.Windows.Forms.CheckBox
$cp6 = New-Object System.Windows.Forms.CheckBox
$cp5 = New-Object System.Windows.Forms.CheckBox
$cp4 = New-Object System.Windows.Forms.CheckBox
$cp3 = New-Object System.Windows.Forms.CheckBox
$cp2 = New-Object System.Windows.Forms.CheckBox
$cp1 = New-Object System.Windows.Forms.CheckBox
$tabConfig = New-Object System.Windows.Forms.TabPage
$grpOptions = New-Object System.Windows.Forms.GroupBox
$lblStreamDay = New-Object System.Windows.Forms.Label
$dropStreamDay = New-Object System.Windows.Forms.ComboBox
$grpOBS = New-Object System.Windows.Forms.GroupBox
$btnOBSReload = New-Object System.Windows.Forms.Button
$dropOBSSource = New-Object System.Windows.Forms.ComboBox
$lblOBSSource = New-Object System.Windows.Forms.Label
$lblOBSScene = New-Object System.Windows.Forms.Label
$dropOBSScene = New-Object System.Windows.Forms.ComboBox
$btnOBSConnect = New-Object System.Windows.Forms.Button
$pnlOBS = New-Object System.Windows.Forms.Panel
$lblOBSStatus = New-Object System.Windows.Forms.Label
$btnConfCancel = New-Object System.Windows.Forms.Button
$btnConfSave = New-Object System.Windows.Forms.Button
$grpConfCountdown = New-Object System.Windows.Forms.GroupBox
$nConfAm = New-Object System.Windows.Forms.DomainUpDown
$nConfMinute = New-Object System.Windows.Forms.NumericUpDown
$nConfHour = New-Object System.Windows.Forms.NumericUpDown
$lblConfDefaultTime = New-Object System.Windows.Forms.Label
$grpConfAudio = New-Object System.Windows.Forms.GroupBox
$txtConfAudio10 = New-Object System.Windows.Forms.TextBox
$txtConfAudio9 = New-Object System.Windows.Forms.TextBox
$txtConfAudio8 = New-Object System.Windows.Forms.TextBox
$txtConfAudio7 = New-Object System.Windows.Forms.TextBox
$txtConfAudio6 = New-Object System.Windows.Forms.TextBox
$txtConfAudio5 = New-Object System.Windows.Forms.TextBox
$txtConfAudio4 = New-Object System.Windows.Forms.TextBox
$txtConfAudio3 = New-Object System.Windows.Forms.TextBox
$lblConfAudio10 = New-Object System.Windows.Forms.Label
$lblConfAudio9 = New-Object System.Windows.Forms.Label
$lblConfAudio8 = New-Object System.Windows.Forms.Label
$lblConfAudio7 = New-Object System.Windows.Forms.Label
$lblConfAudio6 = New-Object System.Windows.Forms.Label
$lblConfAudio5 = New-Object System.Windows.Forms.Label
$lblConfAudio4 = New-Object System.Windows.Forms.Label
$lblConfAudio3 = New-Object System.Windows.Forms.Label
$txtConfAudio2 = New-Object System.Windows.Forms.TextBox
$lblConfAudio2 = New-Object System.Windows.Forms.Label
$txtConfAudio1 = New-Object System.Windows.Forms.TextBox
$lblConfAudio1 = New-Object System.Windows.Forms.Label
$grpConfStream = New-Object System.Windows.Forms.GroupBox
$txtConfStream10 = New-Object System.Windows.Forms.TextBox
$lblConfStream10 = New-Object System.Windows.Forms.Label
$txtConfStream9 = New-Object System.Windows.Forms.TextBox
$txtConfStream8 = New-Object System.Windows.Forms.TextBox
$txtConfStream7 = New-Object System.Windows.Forms.TextBox
$txtConfStream6 = New-Object System.Windows.Forms.TextBox
$txtConfStream5 = New-Object System.Windows.Forms.TextBox
$txtConfStream4 = New-Object System.Windows.Forms.TextBox
$txtConfStream3 = New-Object System.Windows.Forms.TextBox
$txtConfStream2 = New-Object System.Windows.Forms.TextBox
$lblConfStream9 = New-Object System.Windows.Forms.Label
$lblConfStream8 = New-Object System.Windows.Forms.Label
$lblConfStream7 = New-Object System.Windows.Forms.Label
$lblConfStream6 = New-Object System.Windows.Forms.Label
$lblConfStream5 = New-Object System.Windows.Forms.Label
$lblConfStream4 = New-Object System.Windows.Forms.Label
$lblConfStream3 = New-Object System.Windows.Forms.Label
$lblConfStream2 = New-Object System.Windows.Forms.Label
$txtConfStream1 = New-Object System.Windows.Forms.TextBox
$lblConfStream1 = New-Object System.Windows.Forms.Label
$grpConfPrep = New-Object System.Windows.Forms.GroupBox
$txtConfPrep10 = New-Object System.Windows.Forms.TextBox
$lblConfPrep10 = New-Object System.Windows.Forms.Label
$txtConfPrep9 = New-Object System.Windows.Forms.TextBox
$txtConfPrep8 = New-Object System.Windows.Forms.TextBox
$txtConfPrep7 = New-Object System.Windows.Forms.TextBox
$txtConfPrep6 = New-Object System.Windows.Forms.TextBox
$txtConfPrep5 = New-Object System.Windows.Forms.TextBox
$txtConfPrep4 = New-Object System.Windows.Forms.TextBox
$txtConfPrep3 = New-Object System.Windows.Forms.TextBox
$txtConfPrep2 = New-Object System.Windows.Forms.TextBox
$lblConfPrep9 = New-Object System.Windows.Forms.Label
$lblConfPrep8 = New-Object System.Windows.Forms.Label
$lblConfPrep7 = New-Object System.Windows.Forms.Label
$lblConfPrep6 = New-Object System.Windows.Forms.Label
$lblConfPrep5 = New-Object System.Windows.Forms.Label
$lblConfPrep4 = New-Object System.Windows.Forms.Label
$lblConfPrep3 = New-Object System.Windows.Forms.Label
$lblConfPrep2 = New-Object System.Windows.Forms.Label
$lblConfPrep1 = New-Object System.Windows.Forms.Label
$txtConfPrep1 = New-Object System.Windows.Forms.TextBox
$InitialFormWindowState = New-Object System.Windows.Forms.FormWindowState
#endregion Generated Form Objects

# Multi-threaded controls
$SyncHash.lblCountdown = New-Object System.Windows.Forms.Label
$SyncHash.nHour = New-Object System.Windows.Forms.NumericUpDown
$SyncHash.nMinute = New-Object System.Windows.Forms.NumericUpDown
$SyncHash.nAm = New-Object System.Windows.Forms.DomainUpDown


#----------------------------------------------
# OnClick
#----------------------------------------------
# Buttons
$btnCountdownStart_OnClick= {startCountdown}
$btnCountdownStop_OnClick= {stopCountdown}
$btnRefShow_OnClick= {ShowReference}
$btnRefHide_OnClick= {HideReference}
$btnRefLock_OnClick= {ToggleReferenceLock}
$btnTitlePrevious_OnClick= {selectPreviousTitle}
$btnTitleNext_OnClick= {selectNextTitle}
$btnTitlesEdit_OnClick= {toggleTitleEditor}
$btnOBSConnect_OnClick= {OBSConnect}
$btnConfCancel_OnClick= {}
$btnConfSave_OnClick= {SaveAll}
$btnOBSReload_OnClick= {LoadOBSSources}

# Other Controls
$handler_groupBox2_Enter={} 
$handler_checkBox3_CheckedChanged= {}
$handler_form1_Load= {}
$handler_label3_Click= {}
$handler_textBox27_TextChanged= {}
$handler_groupBox1_Enter= {}
$handler_nChapter_TextChanged= {UpdateReference}
$handler_nVerse_TextChanged= {UpdateReference}
$handler_listTitles_SelectedIndexChanged= {SetTitle}
$handler_dropOBSScene_TextChanged= {UpdateSources}
$handler_dropOBSSource_TextChanged= {SetReferenceSource}

$handler_cp1_Click = {checkPrepList}
$handler_cp2_Click = {checkPrepList}
$handler_cp3_Click = {checkPrepList}
$handler_cp4_Click = {checkPrepList}
$handler_cp5_Click = {checkPrepList}
$handler_cp6_Click = {checkPrepList}
$handler_cp7_Click = {checkPrepList}
$handler_cp8_Click = {checkPrepList}
$handler_cp9_Click = {checkPrepList}
$handler_cp10_Click = {checkPrepList}
$cp1.add_Click($handler_cp1_Click)
$cp2.add_Click($handler_cp2_Click)
$cp3.add_Click($handler_cp3_Click)
$cp4.add_Click($handler_cp4_Click)
$cp5.add_Click($handler_cp5_Click)
$cp6.add_Click($handler_cp6_Click)
$cp7.add_Click($handler_cp7_Click)
$cp8.add_Click($handler_cp8_Click)
$cp9.add_Click($handler_cp9_Click)
$cp10.add_Click($handler_cp10_Click)
$handler_cs1_Click = {checkStreamList}
$handler_cs2_Click = {checkStreamList}
$handler_cs3_Click = {checkStreamList}
$handler_cs4_Click = {checkStreamList}
$handler_cs5_Click = {checkStreamList}
$handler_cs6_Click = {checkStreamList}
$handler_cs7_Click = {checkStreamList}
$handler_cs8_Click = {checkStreamList}
$handler_cs9_Click = {checkStreamList}
$handler_cs10_Click = {checkStreamList}
$cs1.add_Click($handler_cs1_Click)
$cs2.add_Click($handler_cs2_Click)
$cs3.add_Click($handler_cs3_Click)
$cs4.add_Click($handler_cs4_Click)
$cs5.add_Click($handler_cs5_Click)
$cs6.add_Click($handler_cs6_Click)
$cs7.add_Click($handler_cs7_Click)
$cs8.add_Click($handler_cs8_Click)
$cs9.add_Click($handler_cs9_Click)
$cs10.add_Click($handler_cs10_Click)
$handler_ca1_Click = {checkAudioList}
$handler_ca2_Click = {checkAudioList}
$handler_ca3_Click = {checkAudioList}
$handler_ca4_Click = {checkAudioList}
$handler_ca5_Click = {checkAudioList}
$handler_ca6_Click = {checkAudioList}
$handler_ca7_Click = {checkAudioList}
$handler_ca8_Click = {checkAudioList}
$handler_ca9_Click = {checkAudioList}
$handler_ca10_Click = {checkAudioList}
$ca1.add_Click($handler_ca1_Click)
$ca2.add_Click($handler_ca2_Click)
$ca3.add_Click($handler_ca3_Click)
$ca4.add_Click($handler_ca4_Click)
$ca5.add_Click($handler_ca5_Click)
$ca6.add_Click($handler_ca6_Click)
$ca7.add_Click($handler_ca7_Click)
$ca8.add_Click($handler_ca8_Click)
$ca9.add_Click($handler_ca9_Click)
$ca10.add_Click($handler_ca10_Click)


# References
$bGen_OnClick= {GetRefBook("Gen")}
$bExo_OnClick= {GetRefBook("Exo")}
$bLev_OnClick= {GetRefBook("Lev")}
$bNum_OnClick= {GetRefBook("Num")}
$bDeu_OnClick= {GetRefBook("Deu")}
$bJos_OnClick= {GetRefBook("Jos")}
$bJdg_OnClick= {GetRefBook("Jdg")}
$bRth_OnClick= {GetRefBook("Rth")}
$b1sa_OnClick= {GetRefBook("1sa")}
$b2sa_OnClick= {GetRefBook("2sa")}
$b1ki_OnClick= {GetRefBook("1ki")}
$b2Ki_OnClick= {GetRefBook("2Ki")}
$b1ch_OnClick= {GetRefBook("1ch")}
$b2ch_OnClick= {GetRefBook("2ch")}
$bEzr_OnClick= {GetRefBook("Ezr")}
$bNeh_OnClick= {GetRefBook("Neh")}
$bEst_OnClick= {GetRefBook("Est")}
$bJob_OnClick= {GetRefBook("Job")}
$bPsa_OnClick= {GetRefBook("Psa")}
$bPro_OnClick= {GetRefBook("Pro")}
$bEcc_OnClick= {GetRefBook("Ecc")}
$bSng_OnClick= {GetRefBook("Sng")}
$bIsa_OnClick= {GetRefBook("Isa")}
$bJer_OnClick= {GetRefBook("Jer")}
$bLam_OnClick= {GetRefBook("Lam")}
$bEze_OnClick= {GetRefBook("Eze")}
$bDan_OnClick= {GetRefBook("Dan")}
$bHos_OnClick= {GetRefBook("Hos")}
$bJoe_OnClick= {GetRefBook("Joe")}
$bAmo_OnClick= {GetRefBook("Amo")}
$bOba_OnClick= {GetRefBook("Oba")}
$bJnh_OnClick= {GetRefBook("Jnh")}
$bMic_OnClick= {GetRefBook("Mic")}
$bNah_OnClick= {GetRefBook("Nah")}
$bHab_OnClick= {GetRefBook("Hab")}
$bZep_OnClick= {GetRefBook("Zep")}
$bHgg_OnClick= {GetRefBook("Hgg")}
$bZec_OnClick= {GetRefBook("Zec")}
$bMal_OnClick= {GetRefBook("Mal")}

$bMtt_OnClick= {GetRefBook("Mtt")}
$bMar_OnClick= {GetRefBook("Mar")}
$bLuk_OnClick= {GetRefBook("Luk")}
$bJhn_OnClick= {GetRefBook("Jhn")}
$bAct_OnClick= {GetRefBook("Act")}
$bRom_OnClick= {GetRefBook("Rom")}
$b1co_OnClick= {GetRefBook("1co")}
$b2co_OnClick= {GetRefBook("2co")} 
$bGal_OnClick= {GetRefBook("Gal")}
$bEph_OnClick= {GetRefBook("Eph")}
$bPhp_OnClick= {GetRefBook("Php")}
$bCol_OnClick= {GetRefBook("Col")}
$b1th_OnClick= {GetRefBook("1th")}
$b2th_OnClick= {GetRefBook("2th")}
$b1ti_OnClick= {GetRefBook("1ti")}
$b2ti_OnClick= {GetRefBook("2ti")}
$bTts_OnClick= {GetRefBook("Tts")}
$bPhm_OnClick= {GetRefBook("Phm")}
$bHeb_OnClick= {GetRefBook("Heb")}
$bJas_OnClick= {GetRefBook("Jas")}
$b1pe_OnClick= {GetRefBook("1pe")}
$b2pe_OnClick= {GetRefBook("2pe")}
$b1jn_OnClick= {GetRefBook("1jn")}
$b2jn_OnClick= {GetRefBook("2jn")}
$b3jn_OnClick= {GetRefBook("3jn")}
$bJud_OnClick= {GetRefBook("Jud")}
$bRev_OnClick= {GetRefBook("Rev")}

$OnLoadForm_StateCorrection=
{#Correct the initial state of the form to prevent the .Net maximized form issue
	$formTOC.WindowState = $InitialFormWindowState
}

# multi-threaded controls definitions

#lblCountdown
$SyncHash.lblCountdown.DataBindings.DefaultDataSourceUpdateMode = 0
$SyncHash.lblCountdown.Font = New-Object System.Drawing.Font("Microsoft Sans Serif",15,0,3,1)

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 93
$SyncHash.lblCountdown.Location = $System_Drawing_Point
$SyncHash.lblCountdown.Name = "lblCountdown"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 34
$System_Drawing_Size.Width = 214
$SyncHash.lblCountdown.Size = $System_Drawing_Size
$SyncHash.lblCountdown.TabIndex = 6
$SyncHash.lblCountdown.Text = "1:30:02"

#nHour
$SyncHash.nHour.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 19
$SyncHash.nHour.Location = $System_Drawing_Point
$SyncHash.nHour.Maximum = 12
$SyncHash.nHour.Name = "nHour"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 43
$SyncHash.nHour.Size = $System_Drawing_Size
$SyncHash.nHour.TabIndex = 3
$SyncHash.nHour.Value = 10

#nMinute
$SyncHash.nMinute.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 55
$System_Drawing_Point.Y = 19
$SyncHash.nMinute.Location = $System_Drawing_Point
$SyncHash.nMinute.Maximum = 59
$SyncHash.nMinute.Name = "nMinute"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 43
$SyncHash.nMinute.Size = $System_Drawing_Size
$SyncHash.nMinute.TabIndex = 4
$SyncHash.nMinute.Value = 30

#nAm - AM / PM selector
$SyncHash.nAm.DataBindings.DefaultDataSourceUpdateMode = 0
$SyncHash.nAm.Items.Add("am")|Out-Null
$SyncHash.nAm.Items.Add("pm")|Out-Null
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 105
$System_Drawing_Point.Y = 19
$SyncHash.nAm.Location = $System_Drawing_Point
$SyncHash.nAm.Name = "nAm"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 49
$SyncHash.nAm.Size = $System_Drawing_Size
$SyncHash.nAm.TabIndex = 5
$SyncHash.nAm.Text = "am"





#----------------------------------------------
#region Generated Form Code
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 446
$System_Drawing_Size.Width = 817
$formTOC.ClientSize = $System_Drawing_Size
$formTOC.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 485
$System_Drawing_Size.Width = 833
$formTOC.MinimumSize = $System_Drawing_Size
$formTOC.Name = "formTOC"
$formTOC.Text = "TOC  -  TBC OBS Controller"
$formTOC.add_Load($handler_form1_Load)

$statusBar.Anchor = 6
$statusBar.DataBindings.DefaultDataSourceUpdateMode = 0
$statusBar.Dock = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 0
$System_Drawing_Point.Y = 425
$statusBar.Location = $System_Drawing_Point
$statusBar.Name = "statusBar"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 22
$System_Drawing_Size.Width = 296
$statusBar.Size = $System_Drawing_Size
$statusBar.SizingGrip = $False
$statusBar.TabIndex = 10
$statusBar.Text = "OBS Offline"

$formTOC.Controls.Add($statusBar)

$lblPrep.Anchor = 10
$lblPrep.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 405
$System_Drawing_Point.Y = 424
$lblPrep.Location = $System_Drawing_Point
$lblPrep.Name = "lblPrep"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 100
$lblPrep.Size = $System_Drawing_Size
$lblPrep.TabIndex = 9
$lblPrep.Text = "Prep"
$lblPrep.TextAlign = 64

$formTOC.Controls.Add($lblPrep)

$pnlPrep.Anchor = 10
$pnlPrep.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)

$pnlPrep.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 511
$System_Drawing_Point.Y = 425
$pnlPrep.Location = $System_Drawing_Point
$pnlPrep.Name = "pnlPrep"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 22
$System_Drawing_Size.Width = 22
$pnlPrep.Size = $System_Drawing_Size
$pnlPrep.TabIndex = 8

$formTOC.Controls.Add($pnlPrep)

$lblStream.Anchor = 10
$lblStream.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 539
$System_Drawing_Point.Y = 424
$lblStream.Location = $System_Drawing_Point
$lblStream.Name = "lblStream"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 64
$lblStream.Size = $System_Drawing_Size
$lblStream.TabIndex = 7
$lblStream.Text = "Stream"
$lblStream.TextAlign = 64

$formTOC.Controls.Add($lblStream)

$pnlStream.Anchor = 10
$pnlStream.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)

$pnlStream.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 609
$System_Drawing_Point.Y = 424
$pnlStream.Location = $System_Drawing_Point
$pnlStream.Name = "pnlStream"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 22
$System_Drawing_Size.Width = 22
$pnlStream.Size = $System_Drawing_Size
$pnlStream.TabIndex = 6

$formTOC.Controls.Add($pnlStream)

$lblAudio.Anchor = 10
$lblAudio.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 637
$System_Drawing_Point.Y = 424
$lblAudio.Location = $System_Drawing_Point
$lblAudio.Name = "lblAudio"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 50
$lblAudio.Size = $System_Drawing_Size
$lblAudio.TabIndex = 5
$lblAudio.Text = "Audio"
$lblAudio.TextAlign = 64

$formTOC.Controls.Add($lblAudio)

$lblAllReady.Anchor = 10
$lblAllReady.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 721
$System_Drawing_Point.Y = 424
$lblAllReady.Location = $System_Drawing_Point
$lblAllReady.Name = "lblAllReady"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 68
$lblAllReady.Size = $System_Drawing_Size
$lblAllReady.TabIndex = 4
$lblAllReady.Text = "All Ready"
$lblAllReady.TextAlign = 64

$formTOC.Controls.Add($lblAllReady)

$pnlAudio.Anchor = 10
$pnlAudio.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)

$pnlAudio.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 693
$System_Drawing_Point.Y = 424
$pnlAudio.Location = $System_Drawing_Point
$pnlAudio.Name = "pnlAudio"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 22
$System_Drawing_Size.Width = 22
$pnlAudio.Size = $System_Drawing_Size
$pnlAudio.TabIndex = 3

$formTOC.Controls.Add($pnlAudio)

$pnlReady.Anchor = 10
$pnlReady.BackColor = [System.Drawing.Color]::FromArgb(255,105,105,105)

$pnlReady.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 795
$System_Drawing_Point.Y = 425
$pnlReady.Location = $System_Drawing_Point
$pnlReady.Name = "pnlReady"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 22
$System_Drawing_Size.Width = 22
$pnlReady.Size = $System_Drawing_Size
$pnlReady.TabIndex = 2

$formTOC.Controls.Add($pnlReady)

$tabMain.Anchor = 15
$tabMain.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 0
$System_Drawing_Point.Y = 7
$tabMain.Location = $System_Drawing_Point
$tabMain.Name = "tabMain"
$tabMain.SelectedIndex = 0
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 411
$System_Drawing_Size.Width = 817
$tabMain.Size = $System_Drawing_Size
$tabMain.TabIndex = 1

$formTOC.Controls.Add($tabMain)
$tabMainControls.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 4
$System_Drawing_Point.Y = 22
$tabMainControls.Location = $System_Drawing_Point
$tabMainControls.Name = "tabMainControls"
$System_Windows_Forms_Padding = New-Object System.Windows.Forms.Padding
$System_Windows_Forms_Padding.All = 3
$System_Windows_Forms_Padding.Bottom = 3
$System_Windows_Forms_Padding.Left = 3
$System_Windows_Forms_Padding.Right = 3
$System_Windows_Forms_Padding.Top = 3
$tabMainControls.Padding = $System_Windows_Forms_Padding
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 385
$System_Drawing_Size.Width = 809
$tabMainControls.Size = $System_Drawing_Size
$tabMainControls.TabIndex = 0
$tabMainControls.Text = "Controls"
$tabMainControls.UseVisualStyleBackColor = $True

$tabMain.Controls.Add($tabMainControls)
$grpRef.Anchor = 15

$grpRef.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 8
$System_Drawing_Point.Y = 143
$grpRef.Location = $System_Drawing_Point
$grpRef.Name = "grpRef"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 236
$System_Drawing_Size.Width = 793
$grpRef.Size = $System_Drawing_Size
$grpRef.TabIndex = 2
$grpRef.TabStop = $False
$grpRef.Text = "Scripture References"

$tabMainControls.Controls.Add($grpRef)
$nVerse.Anchor = 9
$nVerse.DataBindings.DefaultDataSourceUpdateMode = 0
$nVerse.Font = New-Object System.Drawing.Font("Microsoft Sans Serif",15,0,3,1)
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 476
$System_Drawing_Point.Y = 146
$nVerse.Location = $System_Drawing_Point
$nVerse.Maximum = 176
$nVerse.Minimum = 1
$nVerse.Name = "nVerse"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 30
$System_Drawing_Size.Width = 68
$nVerse.Size = $System_Drawing_Size
$nVerse.TabIndex = 78
$nVerse.Value = 1
$nVerse.add_TextChanged($handler_nVerse_TextChanged)

$grpRef.Controls.Add($nVerse)

$lblVerse.Anchor = 9
$lblVerse.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 476
$System_Drawing_Point.Y = 120
$lblVerse.Location = $System_Drawing_Point
$lblVerse.Name = "lblVerse"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 84
$lblVerse.Size = $System_Drawing_Size
$lblVerse.TabIndex = 77
$lblVerse.Text = "Verse"
$lblVerse.TextAlign = 256

$grpRef.Controls.Add($lblVerse)

$lblChapter.Anchor = 9
$lblChapter.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 476
$System_Drawing_Point.Y = 55
$lblChapter.Location = $System_Drawing_Point
$lblChapter.Name = "lblChapter"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 84
$lblChapter.Size = $System_Drawing_Size
$lblChapter.TabIndex = 76
$lblChapter.Text = "Chapter"
$lblChapter.TextAlign = 256
$lblChapter.add_Click($handler_label3_Click)

$grpRef.Controls.Add($lblChapter)

$nChapter.Anchor = 9
$nChapter.DataBindings.DefaultDataSourceUpdateMode = 0
$nChapter.Font = New-Object System.Drawing.Font("Microsoft Sans Serif",15,0,3,1)
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 476
$System_Drawing_Point.Y = 82
$nChapter.Location = $System_Drawing_Point
$nChapter.Maximum = 150
$nChapter.Minimum = 1
$nChapter.Name = "nChapter"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 30
$System_Drawing_Size.Width = 68
$nChapter.Size = $System_Drawing_Size
$nChapter.TabIndex = 75
$nChapter.Value = 1
$nChapter.add_TextChanged($handler_nChapter_TextChanged)

$grpRef.Controls.Add($nChapter)

$bRev.BackColor = [System.Drawing.Color]::FromArgb(255,238,130,238)

$bRev.DataBindings.DefaultDataSourceUpdateMode = 0
$bRev.FlatAppearance.BorderSize = 0
$bRev.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 433
$System_Drawing_Point.Y = 73
$bRev.Location = $System_Drawing_Point
$bRev.Name = "bRev"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bRev.Size = $System_Drawing_Size
$bRev.TabIndex = 74
$bRev.Text = "Rev"
$bRev.UseVisualStyleBackColor = $False
$bRev.add_Click($bRev_OnClick)

$grpRef.Controls.Add($bRev)

$bJud.BackColor = [System.Drawing.Color]::FromArgb(255,0,206,209)

$bJud.DataBindings.DefaultDataSourceUpdateMode = 0
$bJud.FlatAppearance.BorderSize = 0
$bJud.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 433
$System_Drawing_Point.Y = 55
$bJud.Location = $System_Drawing_Point
$bJud.Name = "bJud"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJud.Size = $System_Drawing_Size
$bJud.TabIndex = 73
$bJud.Text = "Jud"
$bJud.UseVisualStyleBackColor = $False
$bJud.add_Click($bJud_OnClick)

$grpRef.Controls.Add($bJud)

$b3jn.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$b3jn.DataBindings.DefaultDataSourceUpdateMode = 0
$b3jn.FlatAppearance.BorderSize = 0
$b3jn.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 395
$System_Drawing_Point.Y = 163
$b3jn.Location = $System_Drawing_Point
$b3jn.Name = "b3jn"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b3jn.Size = $System_Drawing_Size
$b3jn.TabIndex = 72
$b3jn.Text = "3 Jn"
$b3jn.UseVisualStyleBackColor = $False
$b3jn.add_Click($b3jn_OnClick)

$grpRef.Controls.Add($b3jn)

$b2jn.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$b2jn.DataBindings.DefaultDataSourceUpdateMode = 0
$b2jn.FlatAppearance.BorderSize = 0
$b2jn.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 395
$System_Drawing_Point.Y = 145
$b2jn.Location = $System_Drawing_Point
$b2jn.Name = "b2jn"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b2jn.Size = $System_Drawing_Size
$b2jn.TabIndex = 71
$b2jn.Text = "2 Jn"
$b2jn.UseVisualStyleBackColor = $False
$b2jn.add_Click($b2jn_OnClick)

$grpRef.Controls.Add($b2jn)

$b1jn.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$b1jn.DataBindings.DefaultDataSourceUpdateMode = 0
$b1jn.FlatAppearance.BorderSize = 0
$b1jn.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 395
$System_Drawing_Point.Y = 127
$b1jn.Location = $System_Drawing_Point
$b1jn.Name = "b1jn"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b1jn.Size = $System_Drawing_Size
$b1jn.TabIndex = 70
$b1jn.Text = "1 Jn"
$b1jn.UseVisualStyleBackColor = $False
$b1jn.add_Click($b1jn_OnClick)

$grpRef.Controls.Add($b1jn)

$b2pe.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$b2pe.DataBindings.DefaultDataSourceUpdateMode = 0
$b2pe.FlatAppearance.BorderSize = 0
$b2pe.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 395
$System_Drawing_Point.Y = 109
$b2pe.Location = $System_Drawing_Point
$b2pe.Name = "b2pe"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b2pe.Size = $System_Drawing_Size
$b2pe.TabIndex = 69
$b2pe.Text = "2 Pe"
$b2pe.UseVisualStyleBackColor = $False
$b2pe.add_Click($b2pe_OnClick)

$grpRef.Controls.Add($b2pe)

$b1pe.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$b1pe.DataBindings.DefaultDataSourceUpdateMode = 0
$b1pe.FlatAppearance.BorderSize = 0
$b1pe.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 395
$System_Drawing_Point.Y = 91
$b1pe.Location = $System_Drawing_Point
$b1pe.Name = "b1pe"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b1pe.Size = $System_Drawing_Size
$b1pe.TabIndex = 68
$b1pe.Text = "1 Pe"
$b1pe.UseVisualStyleBackColor = $False
$b1pe.add_Click($b1pe_OnClick)

$grpRef.Controls.Add($b1pe)

$bJas.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$bJas.DataBindings.DefaultDataSourceUpdateMode = 0
$bJas.FlatAppearance.BorderSize = 0
$bJas.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 395
$System_Drawing_Point.Y = 73
$bJas.Location = $System_Drawing_Point
$bJas.Name = "bJas"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJas.Size = $System_Drawing_Size
$bJas.TabIndex = 67
$bJas.Text = "Jas"
$bJas.UseVisualStyleBackColor = $False
$bJas.add_Click($bJas_OnClick)

$grpRef.Controls.Add($bJas)

$bHeb.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$bHeb.DataBindings.DefaultDataSourceUpdateMode = 0
$bHeb.FlatAppearance.BorderSize = 0
$bHeb.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 395
$System_Drawing_Point.Y = 55
$bHeb.Location = $System_Drawing_Point
$bHeb.Name = "bHeb"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bHeb.Size = $System_Drawing_Size
$bHeb.TabIndex = 66
$bHeb.Text = "Heb"
$bHeb.UseVisualStyleBackColor = $False
$bHeb.add_Click($bHeb_OnClick)

$grpRef.Controls.Add($bHeb)

$bPhm.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$bPhm.DataBindings.DefaultDataSourceUpdateMode = 0
$bPhm.FlatAppearance.BorderSize = 0
$bPhm.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 357
$System_Drawing_Point.Y = 145
$bPhm.Location = $System_Drawing_Point
$bPhm.Name = "bPhm"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bPhm.Size = $System_Drawing_Size
$bPhm.TabIndex = 65
$bPhm.Text = "Phm"
$bPhm.UseVisualStyleBackColor = $False
$bPhm.add_Click($bPhm_OnClick)

$grpRef.Controls.Add($bPhm)

$bTts.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$bTts.DataBindings.DefaultDataSourceUpdateMode = 0
$bTts.FlatAppearance.BorderSize = 0
$bTts.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 357
$System_Drawing_Point.Y = 127
$bTts.Location = $System_Drawing_Point
$bTts.Name = "bTts"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bTts.Size = $System_Drawing_Size
$bTts.TabIndex = 64
$bTts.Text = "Tts"
$bTts.UseVisualStyleBackColor = $False
$bTts.add_Click($bTts_OnClick)

$grpRef.Controls.Add($bTts)

$b2ti.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$b2ti.DataBindings.DefaultDataSourceUpdateMode = 0
$b2ti.FlatAppearance.BorderSize = 0
$b2ti.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 357
$System_Drawing_Point.Y = 109
$b2ti.Location = $System_Drawing_Point
$b2ti.Name = "b2ti"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b2ti.Size = $System_Drawing_Size
$b2ti.TabIndex = 63
$b2ti.Text = "2 Ti"
$b2ti.UseVisualStyleBackColor = $False
$b2ti.add_Click($b2ti_OnClick)

$grpRef.Controls.Add($b2ti)

$b1ti.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$b1ti.DataBindings.DefaultDataSourceUpdateMode = 0
$b1ti.FlatAppearance.BorderSize = 0
$b1ti.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 357
$System_Drawing_Point.Y = 91
$b1ti.Location = $System_Drawing_Point
$b1ti.Name = "b1ti"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b1ti.Size = $System_Drawing_Size
$b1ti.TabIndex = 62
$b1ti.Text = "1 Ti"
$b1ti.UseVisualStyleBackColor = $False
$b1ti.add_Click($b1ti_OnClick)

$grpRef.Controls.Add($b1ti)

$b2th.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$b2th.DataBindings.DefaultDataSourceUpdateMode = 0
$b2th.FlatAppearance.BorderSize = 0
$b2th.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 357
$System_Drawing_Point.Y = 73
$b2th.Location = $System_Drawing_Point
$b2th.Name = "b2th"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b2th.Size = $System_Drawing_Size
$b2th.TabIndex = 61
$b2th.Text = "2 Th"
$b2th.UseVisualStyleBackColor = $False
$b2th.add_Click($b2th_OnClick)

$grpRef.Controls.Add($b2th)

$b1th.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$b1th.DataBindings.DefaultDataSourceUpdateMode = 0
$b1th.FlatAppearance.BorderSize = 0
$b1th.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 357
$System_Drawing_Point.Y = 55
$b1th.Location = $System_Drawing_Point
$b1th.Name = "b1th"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b1th.Size = $System_Drawing_Size
$b1th.TabIndex = 60
$b1th.Text = "1 Th"
$b1th.UseVisualStyleBackColor = $False
$b1th.add_Click($b1th_OnClick)

$grpRef.Controls.Add($b1th)

$bCol.BackColor = [System.Drawing.Color]::FromArgb(255,210,180,140)

$bCol.DataBindings.DefaultDataSourceUpdateMode = 0
$bCol.FlatAppearance.BorderSize = 0
$bCol.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 319
$System_Drawing_Point.Y = 163
$bCol.Location = $System_Drawing_Point
$bCol.Name = "bCol"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bCol.Size = $System_Drawing_Size
$bCol.TabIndex = 59
$bCol.Text = "Col"
$bCol.UseVisualStyleBackColor = $False
$bCol.add_Click($bCol_OnClick)

$grpRef.Controls.Add($bCol)

$bPhp.BackColor = [System.Drawing.Color]::FromArgb(255,210,180,140)

$bPhp.DataBindings.DefaultDataSourceUpdateMode = 0
$bPhp.FlatAppearance.BorderSize = 0
$bPhp.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 319
$System_Drawing_Point.Y = 145
$bPhp.Location = $System_Drawing_Point
$bPhp.Name = "bPhp"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bPhp.Size = $System_Drawing_Size
$bPhp.TabIndex = 58
$bPhp.Text = "Php"
$bPhp.UseVisualStyleBackColor = $False
$bPhp.add_Click($bPhp_OnClick)

$grpRef.Controls.Add($bPhp)

$bEph.BackColor = [System.Drawing.Color]::FromArgb(255,210,180,140)

$bEph.DataBindings.DefaultDataSourceUpdateMode = 0
$bEph.FlatAppearance.BorderSize = 0
$bEph.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 319
$System_Drawing_Point.Y = 127
$bEph.Location = $System_Drawing_Point
$bEph.Name = "bEph"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bEph.Size = $System_Drawing_Size
$bEph.TabIndex = 57
$bEph.Text = "Eph"
$bEph.UseVisualStyleBackColor = $False
$bEph.add_Click($bEph_OnClick)

$grpRef.Controls.Add($bEph)

$bGal.BackColor = [System.Drawing.Color]::FromArgb(255,210,180,140)

$bGal.DataBindings.DefaultDataSourceUpdateMode = 0
$bGal.FlatAppearance.BorderSize = 0
$bGal.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 319
$System_Drawing_Point.Y = 109
$bGal.Location = $System_Drawing_Point
$bGal.Name = "bGal"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bGal.Size = $System_Drawing_Size
$bGal.TabIndex = 56
$bGal.Text = "Gal"
$bGal.UseVisualStyleBackColor = $False
$bGal.add_Click($bGal_OnClick)

$grpRef.Controls.Add($bGal)

$b2co.BackColor = [System.Drawing.Color]::FromArgb(255,222,184,135)

$b2co.DataBindings.DefaultDataSourceUpdateMode = 0
$b2co.FlatAppearance.BorderSize = 0
$b2co.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 319
$System_Drawing_Point.Y = 91
$b2co.Location = $System_Drawing_Point
$b2co.Name = "b2co"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b2co.Size = $System_Drawing_Size
$b2co.TabIndex = 55
$b2co.Text = "2 Co"
$b2co.UseVisualStyleBackColor = $False
$b2co.add_Click($b2co_OnClick)

$grpRef.Controls.Add($b2co)

$b1co.BackColor = [System.Drawing.Color]::FromArgb(255,222,184,135)

$b1co.DataBindings.DefaultDataSourceUpdateMode = 0
$b1co.FlatAppearance.BorderSize = 0
$b1co.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 319
$System_Drawing_Point.Y = 73
$b1co.Location = $System_Drawing_Point
$b1co.Name = "b1co"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b1co.Size = $System_Drawing_Size
$b1co.TabIndex = 54
$b1co.Text = "1 Co"
$b1co.UseVisualStyleBackColor = $False
$b1co.add_Click($b1co_OnClick)

$grpRef.Controls.Add($b1co)

$bRom.BackColor = [System.Drawing.Color]::FromArgb(255,222,184,135)

$bRom.DataBindings.DefaultDataSourceUpdateMode = 0
$bRom.FlatAppearance.BorderSize = 0
$bRom.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 319
$System_Drawing_Point.Y = 55
$bRom.Location = $System_Drawing_Point
$bRom.Name = "bRom"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bRom.Size = $System_Drawing_Size
$bRom.TabIndex = 53
$bRom.Text = "Rom"
$bRom.UseVisualStyleBackColor = $False
$bRom.add_Click($bRom_OnClick)

$grpRef.Controls.Add($bRom)

$bAct.BackColor = [System.Drawing.Color]::FromArgb(255,192,192,192)

$bAct.DataBindings.DefaultDataSourceUpdateMode = 0
$bAct.FlatAppearance.BorderSize = 0
$bAct.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 281
$System_Drawing_Point.Y = 127
$bAct.Location = $System_Drawing_Point
$bAct.Name = "bAct"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bAct.Size = $System_Drawing_Size
$bAct.TabIndex = 52
$bAct.Text = "Act"
$bAct.UseVisualStyleBackColor = $False
$bAct.add_Click($bAct_OnClick)

$grpRef.Controls.Add($bAct)

$bJhn.BackColor = [System.Drawing.Color]::FromArgb(255,250,128,114)

$bJhn.DataBindings.DefaultDataSourceUpdateMode = 0
$bJhn.FlatAppearance.BorderSize = 0
$bJhn.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 281
$System_Drawing_Point.Y = 109
$bJhn.Location = $System_Drawing_Point
$bJhn.Name = "bJhn"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJhn.Size = $System_Drawing_Size
$bJhn.TabIndex = 51
$bJhn.Text = "Jhn"
$bJhn.UseVisualStyleBackColor = $False
$bJhn.add_Click($bJhn_OnClick)

$grpRef.Controls.Add($bJhn)

$bLuk.BackColor = [System.Drawing.Color]::FromArgb(255,250,128,114)

$bLuk.DataBindings.DefaultDataSourceUpdateMode = 0
$bLuk.FlatAppearance.BorderSize = 0
$bLuk.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 281
$System_Drawing_Point.Y = 91
$bLuk.Location = $System_Drawing_Point
$bLuk.Name = "bLuk"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bLuk.Size = $System_Drawing_Size
$bLuk.TabIndex = 50
$bLuk.Text = "Luk"
$bLuk.UseVisualStyleBackColor = $False
$bLuk.add_Click($bLuk_OnClick)

$grpRef.Controls.Add($bLuk)

$bMar.BackColor = [System.Drawing.Color]::FromArgb(255,250,128,114)

$bMar.DataBindings.DefaultDataSourceUpdateMode = 0
$bMar.FlatAppearance.BorderSize = 0
$bMar.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 281
$System_Drawing_Point.Y = 73
$bMar.Location = $System_Drawing_Point
$bMar.Name = "bMar"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bMar.Size = $System_Drawing_Size
$bMar.TabIndex = 49
$bMar.Text = "Mar"
$bMar.UseVisualStyleBackColor = $False
$bMar.add_Click($bMar_OnClick)

$grpRef.Controls.Add($bMar)

$bMtt.BackColor = [System.Drawing.Color]::FromArgb(255,250,128,114)

$bMtt.DataBindings.DefaultDataSourceUpdateMode = 0
$bMtt.FlatAppearance.BorderSize = 0
$bMtt.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 281
$System_Drawing_Point.Y = 55
$bMtt.Location = $System_Drawing_Point
$bMtt.Name = "bMtt"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bMtt.Size = $System_Drawing_Size
$bMtt.TabIndex = 48
$bMtt.Text = "Mtt"
$bMtt.UseVisualStyleBackColor = $False
$bMtt.add_Click($bMtt_OnClick)

$grpRef.Controls.Add($bMtt)

$bMal.BackColor = [System.Drawing.Color]::FromArgb(255,173,255,47)

$bMal.DataBindings.DefaultDataSourceUpdateMode = 0
$bMal.FlatAppearance.BorderSize = 0
$bMal.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 235
$System_Drawing_Point.Y = 145
$bMal.Location = $System_Drawing_Point
$bMal.Name = "bMal"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bMal.Size = $System_Drawing_Size
$bMal.TabIndex = 47
$bMal.Text = "Mal"
$bMal.UseVisualStyleBackColor = $False
$bMal.add_Click($bMal_OnClick)

$grpRef.Controls.Add($bMal)

$bZec.BackColor = [System.Drawing.Color]::FromArgb(255,173,255,47)

$bZec.DataBindings.DefaultDataSourceUpdateMode = 0
$bZec.FlatAppearance.BorderSize = 0
$bZec.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 235
$System_Drawing_Point.Y = 127
$bZec.Location = $System_Drawing_Point
$bZec.Name = "bZec"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bZec.Size = $System_Drawing_Size
$bZec.TabIndex = 46
$bZec.Text = "Zec"
$bZec.UseVisualStyleBackColor = $False
$bZec.add_Click($bZec_OnClick)

$grpRef.Controls.Add($bZec)

$bHgg.BackColor = [System.Drawing.Color]::FromArgb(255,173,255,47)

$bHgg.DataBindings.DefaultDataSourceUpdateMode = 0
$bHgg.FlatAppearance.BorderSize = 0
$bHgg.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 235
$System_Drawing_Point.Y = 109
$bHgg.Location = $System_Drawing_Point
$bHgg.Name = "bHgg"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bHgg.Size = $System_Drawing_Size
$bHgg.TabIndex = 45
$bHgg.Text = "Hgg"
$bHgg.UseVisualStyleBackColor = $False
$bHgg.add_Click($bHgg_OnClick)

$grpRef.Controls.Add($bHgg)

$bZep.BackColor = [System.Drawing.Color]::FromArgb(255,143,188,139)

$bZep.DataBindings.DefaultDataSourceUpdateMode = 0
$bZep.FlatAppearance.BorderSize = 0
$bZep.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 235
$System_Drawing_Point.Y = 91
$bZep.Location = $System_Drawing_Point
$bZep.Name = "bZep"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bZep.Size = $System_Drawing_Size
$bZep.TabIndex = 44
$bZep.Text = "Zep"
$bZep.UseVisualStyleBackColor = $False
$bZep.add_Click($bZep_OnClick)

$grpRef.Controls.Add($bZep)

$bHab.BackColor = [System.Drawing.Color]::FromArgb(255,143,188,139)

$bHab.DataBindings.DefaultDataSourceUpdateMode = 0
$bHab.FlatAppearance.BorderSize = 0
$bHab.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 235
$System_Drawing_Point.Y = 73
$bHab.Location = $System_Drawing_Point
$bHab.Name = "bHab"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bHab.Size = $System_Drawing_Size
$bHab.TabIndex = 43
$bHab.Text = "Hab"
$bHab.UseVisualStyleBackColor = $False
$bHab.add_Click($bHab_OnClick)

$grpRef.Controls.Add($bHab)

$bNah.BackColor = [System.Drawing.Color]::FromArgb(255,143,188,139)

$bNah.DataBindings.DefaultDataSourceUpdateMode = 0
$bNah.FlatAppearance.BorderSize = 0
$bNah.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 235
$System_Drawing_Point.Y = 55
$bNah.Location = $System_Drawing_Point
$bNah.Name = "bNah"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bNah.Size = $System_Drawing_Size
$bNah.TabIndex = 42
$bNah.Text = "Nah"
$bNah.UseVisualStyleBackColor = $False
$bNah.add_Click($bNah_OnClick)

$grpRef.Controls.Add($bNah)

$bMic.BackColor = [System.Drawing.Color]::FromArgb(255,0,255,127)

$bMic.DataBindings.DefaultDataSourceUpdateMode = 0
$bMic.FlatAppearance.BorderSize = 0
$bMic.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 197
$System_Drawing_Point.Y = 145
$bMic.Location = $System_Drawing_Point
$bMic.Name = "bMic"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bMic.Size = $System_Drawing_Size
$bMic.TabIndex = 41
$bMic.Text = "Mic"
$bMic.UseVisualStyleBackColor = $False
$bMic.add_Click($bMic_OnClick)

$grpRef.Controls.Add($bMic)

$bJnh.BackColor = [System.Drawing.Color]::FromArgb(255,0,255,127)

$bJnh.DataBindings.DefaultDataSourceUpdateMode = 0
$bJnh.FlatAppearance.BorderSize = 0
$bJnh.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 197
$System_Drawing_Point.Y = 127
$bJnh.Location = $System_Drawing_Point
$bJnh.Name = "bJnh"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJnh.Size = $System_Drawing_Size
$bJnh.TabIndex = 40
$bJnh.Text = "Jnh"
$bJnh.UseVisualStyleBackColor = $False
$bJnh.add_Click($bJnh_OnClick)

$grpRef.Controls.Add($bJnh)

$bOba.BackColor = [System.Drawing.Color]::FromArgb(255,0,255,127)

$bOba.DataBindings.DefaultDataSourceUpdateMode = 0
$bOba.FlatAppearance.BorderSize = 0
$bOba.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 197
$System_Drawing_Point.Y = 109
$bOba.Location = $System_Drawing_Point
$bOba.Name = "bOba"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bOba.Size = $System_Drawing_Size
$bOba.TabIndex = 39
$bOba.Text = "Oba"
$bOba.UseVisualStyleBackColor = $False
$bOba.add_Click($bOba_OnClick)

$grpRef.Controls.Add($bOba)

$bAmo.BackColor = [System.Drawing.Color]::FromArgb(255,152,251,152)

$bAmo.DataBindings.DefaultDataSourceUpdateMode = 0
$bAmo.FlatAppearance.BorderSize = 0
$bAmo.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 197
$System_Drawing_Point.Y = 91
$bAmo.Location = $System_Drawing_Point
$bAmo.Name = "bAmo"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bAmo.Size = $System_Drawing_Size
$bAmo.TabIndex = 38
$bAmo.Text = "Amo"
$bAmo.UseVisualStyleBackColor = $False
$bAmo.add_Click($bAmo_OnClick)

$grpRef.Controls.Add($bAmo)

$bJoe.BackColor = [System.Drawing.Color]::FromArgb(255,144,238,144)

$bJoe.DataBindings.DefaultDataSourceUpdateMode = 0
$bJoe.FlatAppearance.BorderSize = 0
$bJoe.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 197
$System_Drawing_Point.Y = 73
$bJoe.Location = $System_Drawing_Point
$bJoe.Name = "bJoe"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJoe.Size = $System_Drawing_Size
$bJoe.TabIndex = 37
$bJoe.Text = "Joe"
$bJoe.UseVisualStyleBackColor = $False
$bJoe.add_Click($bJoe_OnClick)

$grpRef.Controls.Add($bJoe)

$bHos.BackColor = [System.Drawing.Color]::FromArgb(255,144,238,144)

$bHos.DataBindings.DefaultDataSourceUpdateMode = 0
$bHos.FlatAppearance.BorderSize = 0
$bHos.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 197
$System_Drawing_Point.Y = 55
$bHos.Location = $System_Drawing_Point
$bHos.Name = "bHos"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bHos.Size = $System_Drawing_Size
$bHos.TabIndex = 36
$bHos.Text = "Hos"
$bHos.UseVisualStyleBackColor = $False
$bHos.add_Click($bHos_OnClick)

$grpRef.Controls.Add($bHos)

$bDan.BackColor = [System.Drawing.Color]::FromArgb(255,255,215,0)

$bDan.DataBindings.DefaultDataSourceUpdateMode = 0
$bDan.FlatAppearance.BorderSize = 0
$bDan.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 159
$System_Drawing_Point.Y = 127
$bDan.Location = $System_Drawing_Point
$bDan.Name = "bDan"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bDan.Size = $System_Drawing_Size
$bDan.TabIndex = 35
$bDan.Text = "Dan"
$bDan.UseVisualStyleBackColor = $False
$bDan.add_Click($bDan_OnClick)

$grpRef.Controls.Add($bDan)

$bEze.BackColor = [System.Drawing.Color]::FromArgb(255,255,215,0)

$bEze.DataBindings.DefaultDataSourceUpdateMode = 0
$bEze.FlatAppearance.BorderSize = 0
$bEze.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 159
$System_Drawing_Point.Y = 109
$bEze.Location = $System_Drawing_Point
$bEze.Name = "bEze"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bEze.Size = $System_Drawing_Size
$bEze.TabIndex = 34
$bEze.Text = "Eze"
$bEze.UseVisualStyleBackColor = $False
$bEze.add_Click($bEze_OnClick)

$grpRef.Controls.Add($bEze)

$bLam.BackColor = [System.Drawing.Color]::FromArgb(255,255,215,0)

$bLam.DataBindings.DefaultDataSourceUpdateMode = 0
$bLam.FlatAppearance.BorderSize = 0
$bLam.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 159
$System_Drawing_Point.Y = 91
$bLam.Location = $System_Drawing_Point
$bLam.Name = "bLam"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bLam.Size = $System_Drawing_Size
$bLam.TabIndex = 33
$bLam.Text = "Lam"
$bLam.UseVisualStyleBackColor = $False
$bLam.add_Click($bLam_OnClick)

$grpRef.Controls.Add($bLam)

$bJer.BackColor = [System.Drawing.Color]::FromArgb(255,255,215,0)

$bJer.DataBindings.DefaultDataSourceUpdateMode = 0
$bJer.FlatAppearance.BorderSize = 0
$bJer.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 159
$System_Drawing_Point.Y = 73
$bJer.Location = $System_Drawing_Point
$bJer.Name = "bJer"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJer.Size = $System_Drawing_Size
$bJer.TabIndex = 32
$bJer.Text = "Jer"
$bJer.UseVisualStyleBackColor = $False
$bJer.add_Click($bJer_OnClick)

$grpRef.Controls.Add($bJer)

$bIsa.BackColor = [System.Drawing.Color]::FromArgb(255,255,215,0)

$bIsa.DataBindings.DefaultDataSourceUpdateMode = 0
$bIsa.FlatAppearance.BorderSize = 0
$bIsa.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 159
$System_Drawing_Point.Y = 55
$bIsa.Location = $System_Drawing_Point
$bIsa.Name = "bIsa"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bIsa.Size = $System_Drawing_Size
$bIsa.TabIndex = 31
$bIsa.Text = "Isa"
$bIsa.UseVisualStyleBackColor = $False
$bIsa.add_Click($bIsa_OnClick)

$grpRef.Controls.Add($bIsa)

$bSng.BackColor = [System.Drawing.Color]::FromArgb(255,147,112,219)

$bSng.DataBindings.DefaultDataSourceUpdateMode = 0
$bSng.FlatAppearance.BorderSize = 0
$bSng.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 121
$System_Drawing_Point.Y = 127
$bSng.Location = $System_Drawing_Point
$bSng.Name = "bSng"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bSng.Size = $System_Drawing_Size
$bSng.TabIndex = 30
$bSng.Text = "Sng"
$bSng.UseVisualStyleBackColor = $False
$bSng.add_Click($bSng_OnClick)

$grpRef.Controls.Add($bSng)

$bEcc.BackColor = [System.Drawing.Color]::FromArgb(255,147,112,219)

$bEcc.DataBindings.DefaultDataSourceUpdateMode = 0
$bEcc.FlatAppearance.BorderSize = 0
$bEcc.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 121
$System_Drawing_Point.Y = 109
$bEcc.Location = $System_Drawing_Point
$bEcc.Name = "bEcc"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bEcc.Size = $System_Drawing_Size
$bEcc.TabIndex = 29
$bEcc.Text = "Ecc"
$bEcc.UseVisualStyleBackColor = $False
$bEcc.add_Click($bEcc_OnClick)

$grpRef.Controls.Add($bEcc)

$bPro.BackColor = [System.Drawing.Color]::FromArgb(255,147,112,219)

$bPro.DataBindings.DefaultDataSourceUpdateMode = 0
$bPro.FlatAppearance.BorderSize = 0
$bPro.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 121
$System_Drawing_Point.Y = 91
$bPro.Location = $System_Drawing_Point
$bPro.Name = "bPro"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bPro.Size = $System_Drawing_Size
$bPro.TabIndex = 28
$bPro.Text = "Pro"
$bPro.UseVisualStyleBackColor = $False
$bPro.add_Click($bPro_OnClick)

$grpRef.Controls.Add($bPro)

$bPsa.BackColor = [System.Drawing.Color]::FromArgb(255,147,112,219)

$bPsa.DataBindings.DefaultDataSourceUpdateMode = 0
$bPsa.FlatAppearance.BorderSize = 0
$bPsa.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 121
$System_Drawing_Point.Y = 73
$bPsa.Location = $System_Drawing_Point
$bPsa.Name = "bPsa"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bPsa.Size = $System_Drawing_Size
$bPsa.TabIndex = 27
$bPsa.Text = "Psa"
$bPsa.UseVisualStyleBackColor = $False
$bPsa.add_Click($bPsa_OnClick)

$grpRef.Controls.Add($bPsa)

$bJob.BackColor = [System.Drawing.Color]::FromArgb(255,147,112,219)

$bJob.DataBindings.DefaultDataSourceUpdateMode = 0
$bJob.FlatAppearance.BorderSize = 0
$bJob.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 121
$System_Drawing_Point.Y = 55
$bJob.Location = $System_Drawing_Point
$bJob.Name = "bJob"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJob.Size = $System_Drawing_Size
$bJob.TabIndex = 26
$bJob.Text = "Job"
$bJob.UseVisualStyleBackColor = $False
$bJob.add_Click($bJob_OnClick)

$grpRef.Controls.Add($bJob)

$bEst.BackColor = [System.Drawing.Color]::FromArgb(255,135,206,250)

$bEst.DataBindings.DefaultDataSourceUpdateMode = 0
$bEst.FlatAppearance.BorderSize = 0
$bEst.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 83
$System_Drawing_Point.Y = 163
$bEst.Location = $System_Drawing_Point
$bEst.Name = "bEst"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bEst.Size = $System_Drawing_Size
$bEst.TabIndex = 25
$bEst.Text = "Est"
$bEst.UseVisualStyleBackColor = $False
$bEst.add_Click($bEst_OnClick)

$grpRef.Controls.Add($bEst)

$bNeh.BackColor = [System.Drawing.Color]::FromArgb(255,135,206,250)

$bNeh.DataBindings.DefaultDataSourceUpdateMode = 0
$bNeh.FlatAppearance.BorderSize = 0
$bNeh.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 83
$System_Drawing_Point.Y = 145
$bNeh.Location = $System_Drawing_Point
$bNeh.Name = "bNeh"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bNeh.Size = $System_Drawing_Size
$bNeh.TabIndex = 24
$bNeh.Text = "Neh"
$bNeh.UseVisualStyleBackColor = $False
$bNeh.add_Click($bNeh_OnClick)

$grpRef.Controls.Add($bNeh)

$bEzr.BackColor = [System.Drawing.Color]::FromArgb(255,135,206,250)

$bEzr.DataBindings.DefaultDataSourceUpdateMode = 0
$bEzr.FlatAppearance.BorderSize = 0
$bEzr.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 83
$System_Drawing_Point.Y = 127
$bEzr.Location = $System_Drawing_Point
$bEzr.Name = "bEzr"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bEzr.Size = $System_Drawing_Size
$bEzr.TabIndex = 23
$bEzr.Text = "Ezr"
$bEzr.UseVisualStyleBackColor = $False
$bEzr.add_Click($bEzr_OnClick)

$grpRef.Controls.Add($bEzr)

$b2ch.BackColor = [System.Drawing.Color]::FromArgb(255,72,209,204)

$b2ch.DataBindings.DefaultDataSourceUpdateMode = 0
$b2ch.FlatAppearance.BorderSize = 0
$b2ch.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 83
$System_Drawing_Point.Y = 109
$b2ch.Location = $System_Drawing_Point
$b2ch.Name = "b2ch"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b2ch.Size = $System_Drawing_Size
$b2ch.TabIndex = 22
$b2ch.Text = "2 Ch"
$b2ch.UseVisualStyleBackColor = $False
$b2ch.add_Click($b2ch_OnClick)

$grpRef.Controls.Add($b2ch)

$b1ch.BackColor = [System.Drawing.Color]::FromArgb(255,72,209,204)

$b1ch.DataBindings.DefaultDataSourceUpdateMode = 0
$b1ch.FlatAppearance.BorderSize = 0
$b1ch.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 83
$System_Drawing_Point.Y = 91
$b1ch.Location = $System_Drawing_Point
$b1ch.Name = "b1ch"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b1ch.Size = $System_Drawing_Size
$b1ch.TabIndex = 21
$b1ch.Text = "1 Ch"
$b1ch.UseVisualStyleBackColor = $False
$b1ch.add_Click($b1ch_OnClick)

$grpRef.Controls.Add($b1ch)

$b2Ki.BackColor = [System.Drawing.Color]::FromArgb(255,72,209,204)

$b2Ki.DataBindings.DefaultDataSourceUpdateMode = 0
$b2Ki.FlatAppearance.BorderSize = 0
$b2Ki.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 83
$System_Drawing_Point.Y = 73
$b2Ki.Location = $System_Drawing_Point
$b2Ki.Name = "b2Ki"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b2Ki.Size = $System_Drawing_Size
$b2Ki.TabIndex = 20
$b2Ki.Text = "2 Ki"
$b2Ki.UseVisualStyleBackColor = $False
$b2Ki.add_Click($b2Ki_OnClick)

$grpRef.Controls.Add($b2Ki)

$b1ki.BackColor = [System.Drawing.Color]::FromArgb(255,72,209,204)

$b1ki.DataBindings.DefaultDataSourceUpdateMode = 0
$b1ki.FlatAppearance.BorderSize = 0
$b1ki.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 83
$System_Drawing_Point.Y = 55
$b1ki.Location = $System_Drawing_Point
$b1ki.Name = "b1ki"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b1ki.Size = $System_Drawing_Size
$b1ki.TabIndex = 19
$b1ki.Text = "1 Ki"
$b1ki.UseVisualStyleBackColor = $False
$b1ki.add_Click($b1ki_OnClick)

$grpRef.Controls.Add($b1ki)

$b2sa.BackColor = [System.Drawing.Color]::FromArgb(255,72,209,204)

$b2sa.DataBindings.DefaultDataSourceUpdateMode = 0
$b2sa.FlatAppearance.BorderSize = 0
$b2sa.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 45
$System_Drawing_Point.Y = 127
$b2sa.Location = $System_Drawing_Point
$b2sa.Name = "b2sa"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b2sa.Size = $System_Drawing_Size
$b2sa.TabIndex = 18
$b2sa.Text = "2 Sa"
$b2sa.UseVisualStyleBackColor = $False
$b2sa.add_Click($b2sa_OnClick)

$grpRef.Controls.Add($b2sa)

$b1sa.BackColor = [System.Drawing.Color]::FromArgb(255,72,209,204)

$b1sa.DataBindings.DefaultDataSourceUpdateMode = 0
$b1sa.FlatAppearance.BorderSize = 0
$b1sa.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 45
$System_Drawing_Point.Y = 109
$b1sa.Location = $System_Drawing_Point
$b1sa.Name = "b1sa"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$b1sa.Size = $System_Drawing_Size
$b1sa.TabIndex = 17
$b1sa.Text = "1 Sa"
$b1sa.UseVisualStyleBackColor = $False
$b1sa.add_Click($b1sa_OnClick)

$grpRef.Controls.Add($b1sa)

$bRth.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$bRth.DataBindings.DefaultDataSourceUpdateMode = 0
$bRth.FlatAppearance.BorderSize = 0
$bRth.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 45
$System_Drawing_Point.Y = 91
$bRth.Location = $System_Drawing_Point
$bRth.Name = "bRth"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bRth.Size = $System_Drawing_Size
$bRth.TabIndex = 16
$bRth.Text = "Rth"
$bRth.UseVisualStyleBackColor = $False
$bRth.add_Click($bRth_OnClick)

$grpRef.Controls.Add($bRth)

$bJdg.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$bJdg.DataBindings.DefaultDataSourceUpdateMode = 0
$bJdg.FlatAppearance.BorderSize = 0
$bJdg.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 45
$System_Drawing_Point.Y = 73
$bJdg.Location = $System_Drawing_Point
$bJdg.Name = "bJdg"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJdg.Size = $System_Drawing_Size
$bJdg.TabIndex = 15
$bJdg.Text = "Jdg"
$bJdg.UseVisualStyleBackColor = $False
$bJdg.add_Click($bJdg_OnClick)

$grpRef.Controls.Add($bJdg)

$bJos.BackColor = [System.Drawing.Color]::FromArgb(255,176,224,230)

$bJos.DataBindings.DefaultDataSourceUpdateMode = 0
$bJos.FlatAppearance.BorderSize = 0
$bJos.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 45
$System_Drawing_Point.Y = 55
$bJos.Location = $System_Drawing_Point
$bJos.Name = "bJos"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bJos.Size = $System_Drawing_Size
$bJos.TabIndex = 14
$bJos.Text = "Jos"
$bJos.UseVisualStyleBackColor = $False
$bJos.add_Click($bJos_OnClick)

$grpRef.Controls.Add($bJos)

$bDeu.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$bDeu.DataBindings.DefaultDataSourceUpdateMode = 0
$bDeu.FlatAppearance.BorderSize = 0
$bDeu.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 127
$bDeu.Location = $System_Drawing_Point
$bDeu.Name = "bDeu"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bDeu.Size = $System_Drawing_Size
$bDeu.TabIndex = 13
$bDeu.Text = "Deu"
$bDeu.UseVisualStyleBackColor = $False
$bDeu.add_Click($bDeu_OnClick)

$grpRef.Controls.Add($bDeu)

$bNum.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$bNum.DataBindings.DefaultDataSourceUpdateMode = 0
$bNum.FlatAppearance.BorderSize = 0
$bNum.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 109
$bNum.Location = $System_Drawing_Point
$bNum.Name = "bNum"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bNum.Size = $System_Drawing_Size
$bNum.TabIndex = 12
$bNum.Text = "Num"
$bNum.UseVisualStyleBackColor = $False
$bNum.add_Click($bNum_OnClick)

$grpRef.Controls.Add($bNum)

$bLev.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$bLev.DataBindings.DefaultDataSourceUpdateMode = 0
$bLev.FlatAppearance.BorderSize = 0
$bLev.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 91
$bLev.Location = $System_Drawing_Point
$bLev.Name = "bLev"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bLev.Size = $System_Drawing_Size
$bLev.TabIndex = 11
$bLev.Text = "Lev"
$bLev.UseVisualStyleBackColor = $False
$bLev.add_Click($bLev_OnClick)

$grpRef.Controls.Add($bLev)

$bExo.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$bExo.DataBindings.DefaultDataSourceUpdateMode = 0
$bExo.FlatAppearance.BorderSize = 0
$bExo.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 73
$bExo.Location = $System_Drawing_Point
$bExo.Name = "bExo"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bExo.Size = $System_Drawing_Size
$bExo.TabIndex = 10
$bExo.Text = "Exo"
$bExo.UseVisualStyleBackColor = $False
$bExo.add_Click($bExo_OnClick)

$grpRef.Controls.Add($bExo)

$bGen.BackColor = [System.Drawing.Color]::FromArgb(255,244,164,96)

$bGen.DataBindings.DefaultDataSourceUpdateMode = 0
$bGen.FlatAppearance.BorderSize = 0
$bGen.FlatStyle = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 55
$bGen.Location = $System_Drawing_Point
$bGen.Name = "bGen"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 17
$System_Drawing_Size.Width = 37
$bGen.Size = $System_Drawing_Size
$bGen.TabIndex = 9
$bGen.Text = "Gen"
$bGen.UseVisualStyleBackColor = $False
$bGen.add_Click($bGen_OnClick)

$grpRef.Controls.Add($bGen)

$lblRefLive.Anchor = 10
$lblRefLive.DataBindings.DefaultDataSourceUpdateMode = 0
$lblRefLive.Font = New-Object System.Drawing.Font("Microsoft Sans Serif",15,0,3,1)

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 575
$System_Drawing_Point.Y = 121
$lblRefLive.Location = $System_Drawing_Point
$lblRefLive.Name = "lblRefLive"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 35
$System_Drawing_Size.Width = 212
$lblRefLive.Size = $System_Drawing_Size
$lblRefLive.TabIndex = 8
$lblRefLive.Text = "Genesis 2:8"

$grpRef.Controls.Add($lblRefLive)

$lblLiveReferenceLabel.Anchor = 10
$lblLiveReferenceLabel.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 575
$System_Drawing_Point.Y = 98
$lblLiveReferenceLabel.Location = $System_Drawing_Point
$lblLiveReferenceLabel.Name = "lblLiveReferenceLabel"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 165
$lblLiveReferenceLabel.Size = $System_Drawing_Size
$lblLiveReferenceLabel.TabIndex = 7
$lblLiveReferenceLabel.Text = "Live Reference:"
$lblLiveReferenceLabel.TextAlign = 256

$grpRef.Controls.Add($lblLiveReferenceLabel)

$btnRefLock.Anchor = 10
$btnRefLock.BackColor = [System.Drawing.Color]::FromArgb(255,255,99,71)

$btnRefLock.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 575
$System_Drawing_Point.Y = 72
$btnRefLock.Location = $System_Drawing_Point
$btnRefLock.Name = "btnRefLock"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$btnRefLock.Size = $System_Drawing_Size
$btnRefLock.TabIndex = 6
$btnRefLock.Text = "Lock"
$btnRefLock.UseVisualStyleBackColor = $False
$btnRefLock.add_Click($btnRefLock_OnClick)

$grpRef.Controls.Add($btnRefLock)

$btnRefHide.Anchor = 10

$btnRefHide.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 575
$System_Drawing_Point.Y = 168
$btnRefHide.Location = $System_Drawing_Point
$btnRefHide.Name = "btnRefHide"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 62
$System_Drawing_Size.Width = 100
$btnRefHide.Size = $System_Drawing_Size
$btnRefHide.TabIndex = 5
$btnRefHide.Text = "Hide"
$btnRefHide.UseVisualStyleBackColor = $True
$btnRefHide.add_Click($btnRefHide_OnClick)

$grpRef.Controls.Add($btnRefHide)

$btnRefShow.Anchor = 10
$btnRefShow.BackColor = [System.Drawing.Color]::FromArgb(255,176,196,222)

$btnRefShow.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 681
$System_Drawing_Point.Y = 168
$btnRefShow.Location = $System_Drawing_Point
$btnRefShow.Name = "btnRefShow"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 62
$System_Drawing_Size.Width = 106
$btnRefShow.Size = $System_Drawing_Size
$btnRefShow.TabIndex = 4
$btnRefShow.Text = "Show"
$btnRefShow.UseVisualStyleBackColor = $False
$btnRefShow.add_Click($btnRefShow_OnClick)

$grpRef.Controls.Add($btnRefShow)

$lblRefSelected.Anchor = 9
$lblRefSelected.DataBindings.DefaultDataSourceUpdateMode = 0
$lblRefSelected.Font = New-Object System.Drawing.Font("Microsoft Sans Serif",15,0,3,1)

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 575
$System_Drawing_Point.Y = 39
$lblRefSelected.Location = $System_Drawing_Point
$lblRefSelected.Name = "lblRefSelected"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 42
$System_Drawing_Size.Width = 212
$lblRefSelected.Size = $System_Drawing_Size
$lblRefSelected.TabIndex = 3
$lblRefSelected.Text = "Genesis 1:1"

$grpRef.Controls.Add($lblRefSelected)

$lblSelectedReferenceLabel.Anchor = 9
$lblSelectedReferenceLabel.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 575
$System_Drawing_Point.Y = 16
$lblSelectedReferenceLabel.Location = $System_Drawing_Point
$lblSelectedReferenceLabel.Name = "lblSelectedReferenceLabel"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 202
$lblSelectedReferenceLabel.Size = $System_Drawing_Size
$lblSelectedReferenceLabel.TabIndex = 2
$lblSelectedReferenceLabel.Text = "Selected Reference:"
$lblSelectedReferenceLabel.TextAlign = 256

$grpRef.Controls.Add($lblSelectedReferenceLabel)

$txtSearch.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 19
$txtSearch.Location = $System_Drawing_Point
$txtSearch.Name = "txtSearch"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 530
$txtSearch.Size = $System_Drawing_Size
$txtSearch.TabIndex = 1
$txtSearch.Text = "Search for reference"

$grpRef.Controls.Add($txtSearch)


$grpTitles.Anchor = 13

$grpTitles.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 240
$System_Drawing_Point.Y = 6
$grpTitles.Location = $System_Drawing_Point
$grpTitles.Name = "grpTitles"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 131
$System_Drawing_Size.Width = 561
$grpTitles.Size = $System_Drawing_Size
$grpTitles.TabIndex = 1
$grpTitles.TabStop = $False
$grpTitles.Text = "Titles"

$tabMainControls.Controls.Add($grpTitles)
$btnTitlesEdit.Anchor = 9

$btnTitlesEdit.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 490
$System_Drawing_Point.Y = 19
$btnTitlesEdit.Location = $System_Drawing_Point
$btnTitlesEdit.Name = "btnTitlesEdit"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 106
$System_Drawing_Size.Width = 65
$btnTitlesEdit.Size = $System_Drawing_Size
$btnTitlesEdit.TabIndex = 3
$btnTitlesEdit.Text = "Edit"
$btnTitlesEdit.UseVisualStyleBackColor = $True
$btnTitlesEdit.add_Click($btnTitlesEdit_OnClick)

$grpTitles.Controls.Add($btnTitlesEdit)

$btnTitleNext.Anchor = 9

$btnTitleNext.DataBindings.DefaultDataSourceUpdateMode = 0
$btnTitleNext.Font = New-Object System.Drawing.Font("Microsoft Sans Serif",20,0,3,1)

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 424
$System_Drawing_Point.Y = 78
$btnTitleNext.Location = $System_Drawing_Point
$btnTitleNext.Name = "btnTitleNext"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 49
$System_Drawing_Size.Width = 60
$btnTitleNext.Size = $System_Drawing_Size
$btnTitleNext.TabIndex = 2
$btnTitleNext.Text = "↓"
$btnTitleNext.UseVisualStyleBackColor = $True
$btnTitleNext.add_Click($btnTitleNext_OnClick)

$grpTitles.Controls.Add($btnTitleNext)

$btnTitlePrevious.Anchor = 9

$btnTitlePrevious.DataBindings.DefaultDataSourceUpdateMode = 0
$btnTitlePrevious.Font = New-Object System.Drawing.Font("Arial Unicode MS",20,0,3,1)

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 424
$System_Drawing_Point.Y = 19
$btnTitlePrevious.Location = $System_Drawing_Point
$btnTitlePrevious.Name = "btnTitlePrevious"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 49
$System_Drawing_Size.Width = 60
$btnTitlePrevious.Size = $System_Drawing_Size
$btnTitlePrevious.TabIndex = 1
$btnTitlePrevious.Text = "↑"
$btnTitlePrevious.UseVisualStyleBackColor = $True
$btnTitlePrevious.add_Click($btnTitlePrevious_OnClick)

$grpTitles.Controls.Add($btnTitlePrevious)

$listTitles.Anchor = 13
$listTitles.DataBindings.DefaultDataSourceUpdateMode = 0
$listTitles.FormattingEnabled = $True
$listTitles.Items.Add("Service will start at 10:30")|Out-Null
$listTitles.Items.Add("Welcome")|Out-Null
$listTitles.Items.Add("Hymn:")|Out-Null
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 19
$listTitles.Location = $System_Drawing_Point
$listTitles.Name = "listTitles"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 108
$System_Drawing_Size.Width = 412
$listTitles.Size = $System_Drawing_Size
$listTitles.TabIndex = 0
$listTitles.add_SelectedIndexChanged($handler_listTitles_SelectedIndexChanged)

$grpTitles.Controls.Add($listTitles)

$txtTitles.Anchor = 13
$txtTitles.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 19
$txtTitles.Multiline = $True
$txtTitles.Location = $System_Drawing_Point
$txtTitles.Name = "txtTitles"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 108
$System_Drawing_Size.Width = 412
$txtTitles.Size = $System_Drawing_Size
$txtTitles.TabIndex = 0
$txtTitles.Visible = $false

$grpTitles.Controls.Add($txtTitles)


$grpCountdown.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 8
$System_Drawing_Point.Y = 6
$grpCountdown.Location = $System_Drawing_Point
$grpCountdown.Name = "grpCountdown"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 131
$System_Drawing_Size.Width = 226
$grpCountdown.Size = $System_Drawing_Size
$grpCountdown.TabIndex = 0
$grpCountdown.TabStop = $False
$grpCountdown.Text = "Countdown"

$tabMainControls.Controls.Add($grpCountdown)



$btnCountdownStop.BackColor = [System.Drawing.Color]::FromArgb(255,205,92,92)

$btnCountdownStop.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 45
$btnCountdownStop.Location = $System_Drawing_Point
$btnCountdownStop.Name = "btnCountdownStop"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$btnCountdownStop.Size = $System_Drawing_Size
$btnCountdownStop.TabIndex = 2
$btnCountdownStop.Text = "Stop"
$btnCountdownStop.UseVisualStyleBackColor = $False
$btnCountdownStop.add_Click($btnCountdownStop_OnClick)

$grpCountdown.Controls.Add($btnCountdownStop)

$btnCountdownStart.BackColor = [System.Drawing.Color]::FromArgb(255,144,238,144)

$btnCountdownStart.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 87
$System_Drawing_Point.Y = 45
$btnCountdownStart.Location = $System_Drawing_Point
$btnCountdownStart.Name = "btnCountdownStart"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$btnCountdownStart.Size = $System_Drawing_Size
$btnCountdownStart.TabIndex = 1
$btnCountdownStart.Text = "Start"
$btnCountdownStart.UseVisualStyleBackColor = $False
$btnCountdownStart.add_Click($btnCountdownStart_OnClick)

$grpCountdown.Controls.Add($btnCountdownStart)



$tabMainChecklists.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 4
$System_Drawing_Point.Y = 22
$tabMainChecklists.Location = $System_Drawing_Point
$tabMainChecklists.Name = "tabMainChecklists"
$System_Windows_Forms_Padding = New-Object System.Windows.Forms.Padding
$System_Windows_Forms_Padding.All = 3
$System_Windows_Forms_Padding.Bottom = 3
$System_Windows_Forms_Padding.Left = 3
$System_Windows_Forms_Padding.Right = 3
$System_Windows_Forms_Padding.Top = 3
$tabMainChecklists.Padding = $System_Windows_Forms_Padding
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 385
$System_Drawing_Size.Width = 809
$tabMainChecklists.Size = $System_Drawing_Size
$tabMainChecklists.TabIndex = 1
$tabMainChecklists.Text = "Checklists"
$tabMainChecklists.UseVisualStyleBackColor = $True

$tabMain.Controls.Add($tabMainChecklists)
$grpAudio.Anchor = 11

$grpAudio.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 550
$System_Drawing_Point.Y = 6
$grpAudio.Location = $System_Drawing_Point
$grpAudio.Name = "grpAudio"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 373
$System_Drawing_Size.Width = 251
$grpAudio.Size = $System_Drawing_Size
$grpAudio.TabIndex = 2
$grpAudio.TabStop = $False
$grpAudio.Text = "Audio"

$tabMainChecklists.Controls.Add($grpAudio)

$ca10.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 304
$ca10.Location = $System_Drawing_Point
$ca10.Name = "ca10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca10.Size = $System_Drawing_Size
$ca10.TabIndex = 9
$ca10.Text = "checkBox30"
$ca10.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca10)


$ca9.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 274
$ca9.Location = $System_Drawing_Point
$ca9.Name = "ca9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca9.Size = $System_Drawing_Size
$ca9.TabIndex = 8
$ca9.Text = "checkBox29"
$ca9.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca9)


$ca8.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 244
$ca8.Location = $System_Drawing_Point
$ca8.Name = "ca8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca8.Size = $System_Drawing_Size
$ca8.TabIndex = 7
$ca8.Text = "checkBox28"
$ca8.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca8)


$ca7.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 214
$ca7.Location = $System_Drawing_Point
$ca7.Name = "ca7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca7.Size = $System_Drawing_Size
$ca7.TabIndex = 6
$ca7.Text = "checkBox27"
$ca7.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca7)


$ca6.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 184
$ca6.Location = $System_Drawing_Point
$ca6.Name = "ca6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca6.Size = $System_Drawing_Size
$ca6.TabIndex = 5
$ca6.Text = "checkBox26"
$ca6.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca6)


$ca5.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 154
$ca5.Location = $System_Drawing_Point
$ca5.Name = "ca5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca5.Size = $System_Drawing_Size
$ca5.TabIndex = 4
$ca5.Text = "checkBox25"
$ca5.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca5)


$ca4.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 124
$ca4.Location = $System_Drawing_Point
$ca4.Name = "ca4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca4.Size = $System_Drawing_Size
$ca4.TabIndex = 3
$ca4.Text = "checkBox24"
$ca4.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca4)


$ca3.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 94
$ca3.Location = $System_Drawing_Point
$ca3.Name = "ca3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca3.Size = $System_Drawing_Size
$ca3.TabIndex = 2
$ca3.Text = "checkBox23"
$ca3.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca3)


$ca2.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 64
$ca2.Location = $System_Drawing_Point
$ca2.Name = "ca2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 104
$ca2.Size = $System_Drawing_Size
$ca2.TabIndex = 1
$ca2.Text = "checkBox22"
$ca2.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca2)


$ca1.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 34
$ca1.Location = $System_Drawing_Point
$ca1.Name = "ca1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 239
$ca1.Size = $System_Drawing_Size
$ca1.TabIndex = 0
$ca1.Text = "checkBox21"
$ca1.UseVisualStyleBackColor = $True

$grpAudio.Controls.Add($ca1)


$grpStream.Anchor = 15

$grpStream.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 275
$System_Drawing_Point.Y = 6
$grpStream.Location = $System_Drawing_Point
$grpStream.Name = "grpStream"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 373
$System_Drawing_Size.Width = 269
$grpStream.Size = $System_Drawing_Size
$grpStream.TabIndex = 1
$grpStream.TabStop = $False
$grpStream.Text = "Stream"
$grpStream.add_Enter($handler_groupBox2_Enter)

$tabMainChecklists.Controls.Add($grpStream)
$cs9.Anchor = 13

$cs9.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 274
$cs9.Location = $System_Drawing_Point
$cs9.Name = "cs9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs9.Size = $System_Drawing_Size
$cs9.TabIndex = 10
$cs9.Text = "checkBox20"
$cs9.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs9)

$cs10.Anchor = 13

$cs10.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 304
$cs10.Location = $System_Drawing_Point
$cs10.Name = "cs10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs10.Size = $System_Drawing_Size
$cs10.TabIndex = 9
$cs10.Text = "checkBox8"
$cs10.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs10)

$cs8.Anchor = 13

$cs8.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 244
$cs8.Location = $System_Drawing_Point
$cs8.Name = "cs8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs8.Size = $System_Drawing_Size
$cs8.TabIndex = 7
$cs8.Text = "checkBox6"
$cs8.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs8)

$cs7.Anchor = 13

$cs7.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 214
$cs7.Location = $System_Drawing_Point
$cs7.Name = "cs7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs7.Size = $System_Drawing_Size
$cs7.TabIndex = 6
$cs7.Text = "checkBox5"
$cs7.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs7)

$cs6.Anchor = 13

$cs6.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 184
$cs6.Location = $System_Drawing_Point
$cs6.Name = "cs6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs6.Size = $System_Drawing_Size
$cs6.TabIndex = 5
$cs6.Text = "checkBox4"
$cs6.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs6)

$cs5.Anchor = 13

$cs5.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 154
$cs5.Location = $System_Drawing_Point
$cs5.Name = "cs5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs5.Size = $System_Drawing_Size
$cs5.TabIndex = 4
$cs5.Text = "Start Recording"
$cs5.UseVisualStyleBackColor = $True
$cs5.add_CheckedChanged($handler_checkBox3_CheckedChanged)

$grpStream.Controls.Add($cs5)

$cs4.Anchor = 13

$cs4.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 124
$cs4.Location = $System_Drawing_Point
$cs4.Name = "cs4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs4.Size = $System_Drawing_Size
$cs4.TabIndex = 3
$cs4.Text = "Check Mixer Audio in OBS"
$cs4.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs4)

$cs3.Anchor = 13

$cs3.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 94
$cs3.Location = $System_Drawing_Point
$cs3.Name = "cs3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs3.Size = $System_Drawing_Size
$cs3.TabIndex = 2
$cs3.Text = "Start Announcements Slideshow"
$cs3.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs3)

$cs2.Anchor = 13

$cs2.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 64
$cs2.Location = $System_Drawing_Point
$cs2.Name = "cs2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs2.Size = $System_Drawing_Size
$cs2.TabIndex = 1
$cs2.Text = "Set Titles"
$cs2.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs2)

$cs1.Anchor = 13

$cs1.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 34
$cs1.Location = $System_Drawing_Point
$cs1.Name = "cs1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 257
$cs1.Size = $System_Drawing_Size
$cs1.TabIndex = 0
$cs1.Text = "Start Countdown"
$cs1.UseVisualStyleBackColor = $True

$grpStream.Controls.Add($cs1)


$grpPreparation.Anchor = 7

$grpPreparation.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 8
$System_Drawing_Point.Y = 6
$grpPreparation.Location = $System_Drawing_Point
$grpPreparation.Name = "grpPreparation"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 373
$System_Drawing_Size.Width = 261
$grpPreparation.Size = $System_Drawing_Size
$grpPreparation.TabIndex = 0
$grpPreparation.TabStop = $False
$grpPreparation.Text = "Prep"

$tabMainChecklists.Controls.Add($grpPreparation)

$cp10.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 304
$cp10.Location = $System_Drawing_Point
$cp10.Name = "cp10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp10.Size = $System_Drawing_Size
$cp10.TabIndex = 9
$cp10.Text = "checkBox19"
$cp10.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp10)


$cp9.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 274
$cp9.Location = $System_Drawing_Point
$cp9.Name = "cp9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp9.Size = $System_Drawing_Size
$cp9.TabIndex = 8
$cp9.Text = "checkBox18"
$cp9.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp9)


$cp8.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 244
$cp8.Location = $System_Drawing_Point
$cp8.Name = "cp8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp8.Size = $System_Drawing_Size
$cp8.TabIndex = 7
$cp8.Text = "checkBox17"
$cp8.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp8)


$cp7.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 214
$cp7.Location = $System_Drawing_Point
$cp7.Name = "cp7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp7.Size = $System_Drawing_Size
$cp7.TabIndex = 6
$cp7.Text = "checkBox16"
$cp7.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp7)


$cp6.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 184
$cp6.Location = $System_Drawing_Point
$cp6.Name = "cp6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp6.Size = $System_Drawing_Size
$cp6.TabIndex = 5
$cp6.Text = "Set Stream Key in OBS"
$cp6.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp6)


$cp5.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 154
$cp5.Location = $System_Drawing_Point
$cp5.Name = "cp5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp5.Size = $System_Drawing_Size
$cp5.TabIndex = 4
$cp5.Text = "Create Sermon in Wordpress"
$cp5.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp5)


$cp4.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 124
$cp4.Location = $System_Drawing_Point
$cp4.Name = "cp4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp4.Size = $System_Drawing_Size
$cp4.TabIndex = 3
$cp4.Text = "Create Stream in YouTube"
$cp4.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp4)


$cp3.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 94
$cp3.Location = $System_Drawing_Point
$cp3.Name = "cp3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp3.Size = $System_Drawing_Size
$cp3.TabIndex = 2
$cp3.Text = "Verse of the Month"
$cp3.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp3)


$cp2.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 64
$cp2.Location = $System_Drawing_Point
$cp2.Name = "cp2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp2.Size = $System_Drawing_Size
$cp2.TabIndex = 1
$cp2.Text = "Scripture Reading"
$cp2.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp2)


$cp1.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 34
$cp1.Location = $System_Drawing_Point
$cp1.Name = "cp1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 249
$cp1.Size = $System_Drawing_Size
$cp1.TabIndex = 0
$cp1.Text = "Make Song Slides"
$cp1.UseVisualStyleBackColor = $True

$grpPreparation.Controls.Add($cp1)



$tabConfig.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 4
$System_Drawing_Point.Y = 22
$tabConfig.Location = $System_Drawing_Point
$tabConfig.Name = "tabConfig"
$System_Windows_Forms_Padding = New-Object System.Windows.Forms.Padding
$System_Windows_Forms_Padding.All = 3
$System_Windows_Forms_Padding.Bottom = 3
$System_Windows_Forms_Padding.Left = 3
$System_Windows_Forms_Padding.Right = 3
$System_Windows_Forms_Padding.Top = 3
$tabConfig.Padding = $System_Windows_Forms_Padding
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 385
$System_Drawing_Size.Width = 809
$tabConfig.Size = $System_Drawing_Size
$tabConfig.TabIndex = 2
$tabConfig.Text = "Config"
$tabConfig.UseVisualStyleBackColor = $True

$tabMain.Controls.Add($tabConfig)

$grpOptions.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 626
$System_Drawing_Point.Y = 104
$grpOptions.Location = $System_Drawing_Point
$grpOptions.Name = "grpOptions"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 246
$System_Drawing_Size.Width = 169
$grpOptions.Size = $System_Drawing_Size
$grpOptions.TabIndex = 7
$grpOptions.TabStop = $False
$grpOptions.Text = "Options"

$tabConfig.Controls.Add($grpOptions)
$lblStreamDay.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 21
$lblStreamDay.Location = $System_Drawing_Point
$lblStreamDay.Name = "lblStreamDay"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 100
$lblStreamDay.Size = $System_Drawing_Size
$lblStreamDay.TabIndex = 1
$lblStreamDay.Text = "Stream Day"
$lblStreamDay.TextAlign = 16

$grpOptions.Controls.Add($lblStreamDay)

$dropStreamDay.DataBindings.DefaultDataSourceUpdateMode = 0
$dropStreamDay.FormattingEnabled = $True
$dropStreamDay.Items.Add("Sunday")|Out-Null
$dropStreamDay.Items.Add("Monday")|Out-Null
$dropStreamDay.Items.Add("Tuesday")|Out-Null
$dropStreamDay.Items.Add("Wednesday")|Out-Null
$dropStreamDay.Items.Add("Thursday")|Out-Null
$dropStreamDay.Items.Add("Friday")|Out-Null
$dropStreamDay.Items.Add("Saturday")|Out-Null
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 47
$dropStreamDay.Location = $System_Drawing_Point
$dropStreamDay.Name = "dropStreamDay"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 21
$System_Drawing_Size.Width = 121
$dropStreamDay.Size = $System_Drawing_Size
$dropStreamDay.TabIndex = 0
$dropStreamDay.Text = "Sunday"

$grpOptions.Controls.Add($dropStreamDay)



$grpOBS.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 287
$System_Drawing_Point.Y = 6
$grpOBS.Location = $System_Drawing_Point
$grpOBS.Name = "grpOBS"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 92
$System_Drawing_Size.Width = 514
$grpOBS.Size = $System_Drawing_Size
$grpOBS.TabIndex = 6
$grpOBS.TabStop = $False
$grpOBS.Text = "OBS"

$tabConfig.Controls.Add($grpOBS)

$btnOBSReload.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 95
$System_Drawing_Point.Y = 23
$btnOBSReload.Location = $System_Drawing_Point
$btnOBSReload.Name = "btnOBSReload"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 107
$btnOBSReload.Size = $System_Drawing_Size
$btnOBSReload.TabIndex = 6
$btnOBSReload.Text = "Reload Sources"
$btnOBSReload.UseVisualStyleBackColor = $True
$btnOBSReload.add_Click($btnOBSReload_OnClick)

$grpOBS.Controls.Add($btnOBSReload)

$dropOBSSource.DataBindings.DefaultDataSourceUpdateMode = 0
$dropOBSSource.FormattingEnabled = $True
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 368
$System_Drawing_Point.Y = 52
$dropOBSSource.Location = $System_Drawing_Point
$dropOBSSource.Name = "dropOBSSource"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 21
$System_Drawing_Size.Width = 140
$dropOBSSource.Size = $System_Drawing_Size
$dropOBSSource.TabIndex = 5
$dropOBSSource.add_TextChanged($handler_dropOBSSource_TextChanged)

$grpOBS.Controls.Add($dropOBSSource)

$lblOBSSource.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 368
$System_Drawing_Point.Y = 26
$lblOBSSource.Location = $System_Drawing_Point
$lblOBSSource.Name = "lblOBSSource"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 100
$lblOBSSource.Size = $System_Drawing_Size
$lblOBSSource.TabIndex = 4
$lblOBSSource.Text = "Reference Source:"
$lblOBSSource.TextAlign = 16

$grpOBS.Controls.Add($lblOBSSource)

$lblOBSScene.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 222
$System_Drawing_Point.Y = 26
$lblOBSScene.Location = $System_Drawing_Point
$lblOBSScene.Name = "lblOBSScene"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 100
$lblOBSScene.Size = $System_Drawing_Size
$lblOBSScene.TabIndex = 3
$lblOBSScene.Text = "Scene Containing Source:"
$lblOBSScene.TextAlign = 16

$grpOBS.Controls.Add($lblOBSScene)

$dropOBSScene.DataBindings.DefaultDataSourceUpdateMode = 0
$dropOBSScene.FormattingEnabled = $True
$dropOBSScene.Items.Add("Scene name one")|Out-Null
$dropOBSScene.Items.Add("scene name two")|Out-Null
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 222
$System_Drawing_Point.Y = 52
$dropOBSScene.Location = $System_Drawing_Point
$dropOBSScene.Name = "dropOBSScene"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 21
$System_Drawing_Size.Width = 140
$dropOBSScene.Size = $System_Drawing_Size
$dropOBSScene.TabIndex = 2
$dropOBSScene.add_TextChanged($handler_dropOBSScene_TextChanged)

$grpOBS.Controls.Add($dropOBSScene)


$btnOBSConnect.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 14
$System_Drawing_Point.Y = 23
$btnOBSConnect.Location = $System_Drawing_Point
$btnOBSConnect.Name = "btnOBSConnect"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$btnOBSConnect.Size = $System_Drawing_Size
$btnOBSConnect.TabIndex = 1
$btnOBSConnect.Text = "Connect"
$btnOBSConnect.UseVisualStyleBackColor = $True
$btnOBSConnect.add_Click($btnOBSConnect_OnClick)

$grpOBS.Controls.Add($btnOBSConnect)

$pnlOBS.BackColor = [System.Drawing.Color]::FromArgb(255,169,169,169)

$pnlOBS.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 14
$System_Drawing_Point.Y = 52
$pnlOBS.Location = $System_Drawing_Point
$pnlOBS.Name = "pnlOBS"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 188
$pnlOBS.Size = $System_Drawing_Size
$pnlOBS.TabIndex = 0

$grpOBS.Controls.Add($pnlOBS)
$lblOBSStatus.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 0
$System_Drawing_Point.Y = 0
$lblOBSStatus.Location = $System_Drawing_Point
$lblOBSStatus.Name = "lblOBSStatus"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 188
$lblOBSStatus.Size = $System_Drawing_Size
$lblOBSStatus.TabIndex = 0
$lblOBSStatus.Text = "Not connected"
$lblOBSStatus.TextAlign = 16

$pnlOBS.Controls.Add($lblOBSStatus)




$btnConfCancel.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 645
$System_Drawing_Point.Y = 356
$btnConfCancel.Location = $System_Drawing_Point
$btnConfCancel.Name = "btnConfCancel"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$btnConfCancel.Size = $System_Drawing_Size
$btnConfCancel.TabIndex = 5
$btnConfCancel.Text = "Cancel"
$btnConfCancel.UseVisualStyleBackColor = $True
$btnConfCancel.add_Click($btnConfCancel_OnClick)

$tabConfig.Controls.Add($btnConfCancel)


$btnConfSave.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 726
$System_Drawing_Point.Y = 356
$btnConfSave.Location = $System_Drawing_Point
$btnConfSave.Name = "btnConfSave"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 75
$btnConfSave.Size = $System_Drawing_Size
$btnConfSave.TabIndex = 4
$btnConfSave.Text = "Save"
$btnConfSave.UseVisualStyleBackColor = $True
$btnConfSave.add_Click($btnConfSave_OnClick)

$tabConfig.Controls.Add($btnConfSave)


$grpConfCountdown.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 8
$System_Drawing_Point.Y = 6
$grpConfCountdown.Location = $System_Drawing_Point
$grpConfCountdown.Name = "grpConfCountdown"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 92
$System_Drawing_Size.Width = 273
$grpConfCountdown.Size = $System_Drawing_Size
$grpConfCountdown.TabIndex = 3
$grpConfCountdown.TabStop = $False
$grpConfCountdown.Text = "Countdown"

$tabConfig.Controls.Add($grpConfCountdown)
$nConfAm.DataBindings.DefaultDataSourceUpdateMode = 0
$nConfAm.Items.Add("am")|Out-Null
$nConfAm.Items.Add("pm")|Out-Null
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 213
$System_Drawing_Point.Y = 19
$nConfAm.Location = $System_Drawing_Point
$nConfAm.Name = "nConfAm"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 49
$nConfAm.Size = $System_Drawing_Size
$nConfAm.TabIndex = 3
$nConfAm.Text = "am"

$grpConfCountdown.Controls.Add($nConfAm)

$nConfMinute.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 164
$System_Drawing_Point.Y = 19
$nConfMinute.Location = $System_Drawing_Point
$nConfMinute.Maximum = 59
$nConfMinute.Minimum = 1
$nConfMinute.Name = "nConfMinute"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 43
$nConfMinute.Size = $System_Drawing_Size
$nConfMinute.TabIndex = 2
$nConfMinute.Value = 30

$grpConfCountdown.Controls.Add($nConfMinute)

$nConfHour.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 112
$System_Drawing_Point.Y = 19
$nConfHour.Location = $System_Drawing_Point
$nConfHour.Maximum = 12
$nConfHour.Minimum = 1
$nConfHour.Name = "nConfHour"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 43
$nConfHour.Size = $System_Drawing_Size
$nConfHour.TabIndex = 1
$nConfHour.Value = 10

$grpConfCountdown.Controls.Add($nConfHour)

$lblConfDefaultTime.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 16
$lblConfDefaultTime.Location = $System_Drawing_Point
$lblConfDefaultTime.Name = "lblConfDefaultTime"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 100
$lblConfDefaultTime.Size = $System_Drawing_Size
$lblConfDefaultTime.TabIndex = 0
$lblConfDefaultTime.Text = "Default Time:"
$lblConfDefaultTime.TextAlign = 64

$grpConfCountdown.Controls.Add($lblConfDefaultTime)



$grpConfAudio.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 420
$System_Drawing_Point.Y = 104
$grpConfAudio.Location = $System_Drawing_Point
$grpConfAudio.Name = "grpConfAudio"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 275
$System_Drawing_Size.Width = 200
$grpConfAudio.Size = $System_Drawing_Size
$grpConfAudio.TabIndex = 2
$grpConfAudio.TabStop = $False
$grpConfAudio.Text = "Audio"

$tabConfig.Controls.Add($grpConfAudio)
$txtConfAudio10.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 231
$txtConfAudio10.Location = $System_Drawing_Point
$txtConfAudio10.Name = "txtConfAudio10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio10.Size = $System_Drawing_Size
$txtConfAudio10.TabIndex = 55

$grpConfAudio.Controls.Add($txtConfAudio10)

$txtConfAudio9.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 208
$txtConfAudio9.Location = $System_Drawing_Point
$txtConfAudio9.Name = "txtConfAudio9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio9.Size = $System_Drawing_Size
$txtConfAudio9.TabIndex = 54

$grpConfAudio.Controls.Add($txtConfAudio9)

$txtConfAudio8.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 185
$txtConfAudio8.Location = $System_Drawing_Point
$txtConfAudio8.Name = "txtConfAudio8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio8.Size = $System_Drawing_Size
$txtConfAudio8.TabIndex = 53

$grpConfAudio.Controls.Add($txtConfAudio8)

$txtConfAudio7.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 162
$txtConfAudio7.Location = $System_Drawing_Point
$txtConfAudio7.Name = "txtConfAudio7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio7.Size = $System_Drawing_Size
$txtConfAudio7.TabIndex = 52

$grpConfAudio.Controls.Add($txtConfAudio7)

$txtConfAudio6.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 139
$txtConfAudio6.Location = $System_Drawing_Point
$txtConfAudio6.Name = "txtConfAudio6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio6.Size = $System_Drawing_Size
$txtConfAudio6.TabIndex = 51
$txtConfAudio6.add_TextChanged($handler_textBox27_TextChanged)

$grpConfAudio.Controls.Add($txtConfAudio6)

$txtConfAudio5.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 116
$txtConfAudio5.Location = $System_Drawing_Point
$txtConfAudio5.Name = "txtConfAudio5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio5.Size = $System_Drawing_Size
$txtConfAudio5.TabIndex = 50

$grpConfAudio.Controls.Add($txtConfAudio5)

$txtConfAudio4.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 93
$txtConfAudio4.Location = $System_Drawing_Point
$txtConfAudio4.Name = "txtConfAudio4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio4.Size = $System_Drawing_Size
$txtConfAudio4.TabIndex = 49

$grpConfAudio.Controls.Add($txtConfAudio4)

$txtConfAudio3.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 70
$txtConfAudio3.Location = $System_Drawing_Point
$txtConfAudio3.Name = "txtConfAudio3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio3.Size = $System_Drawing_Size
$txtConfAudio3.TabIndex = 48

$grpConfAudio.Controls.Add($txtConfAudio3)

$lblConfAudio10.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 229
$lblConfAudio10.Location = $System_Drawing_Point
$lblConfAudio10.Name = "lblConfAudio10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio10.Size = $System_Drawing_Size
$lblConfAudio10.TabIndex = 47
$lblConfAudio10.Text = "10"
$lblConfAudio10.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio10)

$lblConfAudio9.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 206
$lblConfAudio9.Location = $System_Drawing_Point
$lblConfAudio9.Name = "lblConfAudio9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio9.Size = $System_Drawing_Size
$lblConfAudio9.TabIndex = 46
$lblConfAudio9.Text = "9"
$lblConfAudio9.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio9)

$lblConfAudio8.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 183
$lblConfAudio8.Location = $System_Drawing_Point
$lblConfAudio8.Name = "lblConfAudio8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio8.Size = $System_Drawing_Size
$lblConfAudio8.TabIndex = 45
$lblConfAudio8.Text = "8"
$lblConfAudio8.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio8)

$lblConfAudio7.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 160
$lblConfAudio7.Location = $System_Drawing_Point
$lblConfAudio7.Name = "lblConfAudio7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio7.Size = $System_Drawing_Size
$lblConfAudio7.TabIndex = 44
$lblConfAudio7.Text = "7"
$lblConfAudio7.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio7)

$lblConfAudio6.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 137
$lblConfAudio6.Location = $System_Drawing_Point
$lblConfAudio6.Name = "lblConfAudio6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio6.Size = $System_Drawing_Size
$lblConfAudio6.TabIndex = 43
$lblConfAudio6.Text = "6"
$lblConfAudio6.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio6)

$lblConfAudio5.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 114
$lblConfAudio5.Location = $System_Drawing_Point
$lblConfAudio5.Name = "lblConfAudio5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio5.Size = $System_Drawing_Size
$lblConfAudio5.TabIndex = 42
$lblConfAudio5.Text = "5"
$lblConfAudio5.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio5)

$lblConfAudio4.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 91
$lblConfAudio4.Location = $System_Drawing_Point
$lblConfAudio4.Name = "lblConfAudio4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio4.Size = $System_Drawing_Size
$lblConfAudio4.TabIndex = 41
$lblConfAudio4.Text = "4"
$lblConfAudio4.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio4)

$lblConfAudio3.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 68
$lblConfAudio3.Location = $System_Drawing_Point
$lblConfAudio3.Name = "lblConfAudio3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio3.Size = $System_Drawing_Size
$lblConfAudio3.TabIndex = 40
$lblConfAudio3.Text = "3"
$lblConfAudio3.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio3)

$txtConfAudio2.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 47
$txtConfAudio2.Location = $System_Drawing_Point
$txtConfAudio2.Name = "txtConfAudio2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio2.Size = $System_Drawing_Size
$txtConfAudio2.TabIndex = 39

$grpConfAudio.Controls.Add($txtConfAudio2)

$lblConfAudio2.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 45
$lblConfAudio2.Location = $System_Drawing_Point
$lblConfAudio2.Name = "lblConfAudio2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio2.Size = $System_Drawing_Size
$lblConfAudio2.TabIndex = 38
$lblConfAudio2.Text = "2"
$lblConfAudio2.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio2)

$txtConfAudio1.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 24
$txtConfAudio1.Location = $System_Drawing_Point
$txtConfAudio1.Name = "txtConfAudio1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfAudio1.Size = $System_Drawing_Size
$txtConfAudio1.TabIndex = 37

$grpConfAudio.Controls.Add($txtConfAudio1)

$lblConfAudio1.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 21
$lblConfAudio1.Location = $System_Drawing_Point
$lblConfAudio1.Name = "lblConfAudio1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfAudio1.Size = $System_Drawing_Size
$lblConfAudio1.TabIndex = 28
$lblConfAudio1.Text = "1"
$lblConfAudio1.TextAlign = 32

$grpConfAudio.Controls.Add($lblConfAudio1)



$grpConfStream.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 214
$System_Drawing_Point.Y = 104
$grpConfStream.Location = $System_Drawing_Point
$grpConfStream.Name = "grpConfStream"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 275
$System_Drawing_Size.Width = 200
$grpConfStream.Size = $System_Drawing_Size
$grpConfStream.TabIndex = 1
$grpConfStream.TabStop = $False
$grpConfStream.Text = "Stream"

$tabConfig.Controls.Add($grpConfStream)
$txtConfStream10.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 231
$txtConfStream10.Location = $System_Drawing_Point
$txtConfStream10.Name = "txtConfStream10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream10.Size = $System_Drawing_Size
$txtConfStream10.TabIndex = 38

$grpConfStream.Controls.Add($txtConfStream10)

$lblConfStream10.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 229
$lblConfStream10.Location = $System_Drawing_Point
$lblConfStream10.Name = "lblConfStream10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream10.Size = $System_Drawing_Size
$lblConfStream10.TabIndex = 37
$lblConfStream10.Text = "10"
$lblConfStream10.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream10)

$txtConfStream9.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 208
$txtConfStream9.Location = $System_Drawing_Point
$txtConfStream9.Name = "txtConfStream9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream9.Size = $System_Drawing_Size
$txtConfStream9.TabIndex = 36

$grpConfStream.Controls.Add($txtConfStream9)

$txtConfStream8.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 185
$txtConfStream8.Location = $System_Drawing_Point
$txtConfStream8.Name = "txtConfStream8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream8.Size = $System_Drawing_Size
$txtConfStream8.TabIndex = 35

$grpConfStream.Controls.Add($txtConfStream8)

$txtConfStream7.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 162
$txtConfStream7.Location = $System_Drawing_Point
$txtConfStream7.Name = "txtConfStream7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream7.Size = $System_Drawing_Size
$txtConfStream7.TabIndex = 34

$grpConfStream.Controls.Add($txtConfStream7)

$txtConfStream6.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 139
$txtConfStream6.Location = $System_Drawing_Point
$txtConfStream6.Name = "txtConfStream6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream6.Size = $System_Drawing_Size
$txtConfStream6.TabIndex = 33

$grpConfStream.Controls.Add($txtConfStream6)

$txtConfStream5.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 116
$txtConfStream5.Location = $System_Drawing_Point
$txtConfStream5.Name = "txtConfStream5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream5.Size = $System_Drawing_Size
$txtConfStream5.TabIndex = 32

$grpConfStream.Controls.Add($txtConfStream5)

$txtConfStream4.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 93
$txtConfStream4.Location = $System_Drawing_Point
$txtConfStream4.Name = "txtConfStream4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream4.Size = $System_Drawing_Size
$txtConfStream4.TabIndex = 31

$grpConfStream.Controls.Add($txtConfStream4)

$txtConfStream3.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 70
$txtConfStream3.Location = $System_Drawing_Point
$txtConfStream3.Name = "txtConfStream3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream3.Size = $System_Drawing_Size
$txtConfStream3.TabIndex = 30

$grpConfStream.Controls.Add($txtConfStream3)

$txtConfStream2.DataBindings.DefaultDataSourceUpdateMode = 0
$txtConfStream2.Enabled = $False
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 47
$txtConfStream2.Location = $System_Drawing_Point
$txtConfStream2.Name = "txtConfStream2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream2.Size = $System_Drawing_Size
$txtConfStream2.TabIndex = 29
$txtConfStream2.Text = "Set Titles"

$grpConfStream.Controls.Add($txtConfStream2)

$lblConfStream9.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 206
$lblConfStream9.Location = $System_Drawing_Point
$lblConfStream9.Name = "lblConfStream9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream9.Size = $System_Drawing_Size
$lblConfStream9.TabIndex = 27
$lblConfStream9.Text = "9"
$lblConfStream9.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream9)

$lblConfStream8.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 183
$lblConfStream8.Location = $System_Drawing_Point
$lblConfStream8.Name = "lblConfStream8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream8.Size = $System_Drawing_Size
$lblConfStream8.TabIndex = 26
$lblConfStream8.Text = "8"
$lblConfStream8.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream8)

$lblConfStream7.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 160
$lblConfStream7.Location = $System_Drawing_Point
$lblConfStream7.Name = "lblConfStream7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream7.Size = $System_Drawing_Size
$lblConfStream7.TabIndex = 25
$lblConfStream7.Text = "7"
$lblConfStream7.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream7)

$lblConfStream6.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 137
$lblConfStream6.Location = $System_Drawing_Point
$lblConfStream6.Name = "lblConfStream6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream6.Size = $System_Drawing_Size
$lblConfStream6.TabIndex = 24
$lblConfStream6.Text = "6"
$lblConfStream6.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream6)

$lblConfStream5.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 114
$lblConfStream5.Location = $System_Drawing_Point
$lblConfStream5.Name = "lblConfStream5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream5.Size = $System_Drawing_Size
$lblConfStream5.TabIndex = 23
$lblConfStream5.Text = "5"
$lblConfStream5.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream5)

$lblConfStream4.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 91
$lblConfStream4.Location = $System_Drawing_Point
$lblConfStream4.Name = "lblConfStream4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream4.Size = $System_Drawing_Size
$lblConfStream4.TabIndex = 22
$lblConfStream4.Text = "4"
$lblConfStream4.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream4)

$lblConfStream3.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 68
$lblConfStream3.Location = $System_Drawing_Point
$lblConfStream3.Name = "lblConfStream3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream3.Size = $System_Drawing_Size
$lblConfStream3.TabIndex = 21
$lblConfStream3.Text = "3"
$lblConfStream3.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream3)

$lblConfStream2.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 7
$System_Drawing_Point.Y = 45
$lblConfStream2.Location = $System_Drawing_Point
$lblConfStream2.Name = "lblConfStream2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 29
$lblConfStream2.Size = $System_Drawing_Size
$lblConfStream2.TabIndex = 20
$lblConfStream2.Text = "2"
$lblConfStream2.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream2)

$txtConfStream1.DataBindings.DefaultDataSourceUpdateMode = 0
$txtConfStream1.Enabled = $False
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 24
$txtConfStream1.Location = $System_Drawing_Point
$txtConfStream1.Name = "txtConfStream1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfStream1.Size = $System_Drawing_Size
$txtConfStream1.TabIndex = 19
$txtConfStream1.Text = "Start Countdown"

$grpConfStream.Controls.Add($txtConfStream1)

$lblConfStream1.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 22
$lblConfStream1.Location = $System_Drawing_Point
$lblConfStream1.Name = "lblConfStream1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfStream1.Size = $System_Drawing_Size
$lblConfStream1.TabIndex = 10
$lblConfStream1.Text = "1"
$lblConfStream1.TextAlign = 32

$grpConfStream.Controls.Add($lblConfStream1)



$grpConfPrep.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 8
$System_Drawing_Point.Y = 104
$grpConfPrep.Location = $System_Drawing_Point
$grpConfPrep.Name = "grpConfPrep"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 275
$System_Drawing_Size.Width = 200
$grpConfPrep.Size = $System_Drawing_Size
$grpConfPrep.TabIndex = 0
$grpConfPrep.TabStop = $False
$grpConfPrep.Text = "Prep"
$grpConfPrep.add_Enter($handler_groupBox1_Enter)

$tabConfig.Controls.Add($grpConfPrep)
$txtConfPrep10.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 231
$txtConfPrep10.Location = $System_Drawing_Point
$txtConfPrep10.Name = "txtConfPrep10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep10.Size = $System_Drawing_Size
$txtConfPrep10.TabIndex = 20

$grpConfPrep.Controls.Add($txtConfPrep10)

$lblConfPrep10.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 229
$lblConfPrep10.Location = $System_Drawing_Point
$lblConfPrep10.Name = "lblConfPrep10"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep10.Size = $System_Drawing_Size
$lblConfPrep10.TabIndex = 19
$lblConfPrep10.Text = "10"
$lblConfPrep10.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep10)

$txtConfPrep9.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 208
$txtConfPrep9.Location = $System_Drawing_Point
$txtConfPrep9.Name = "txtConfPrep9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep9.Size = $System_Drawing_Size
$txtConfPrep9.TabIndex = 18

$grpConfPrep.Controls.Add($txtConfPrep9)

$txtConfPrep8.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 185
$txtConfPrep8.Location = $System_Drawing_Point
$txtConfPrep8.Name = "txtConfPrep8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep8.Size = $System_Drawing_Size
$txtConfPrep8.TabIndex = 17

$grpConfPrep.Controls.Add($txtConfPrep8)

$txtConfPrep7.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 162
$txtConfPrep7.Location = $System_Drawing_Point
$txtConfPrep7.Name = "txtConfPrep7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep7.Size = $System_Drawing_Size
$txtConfPrep7.TabIndex = 16

$grpConfPrep.Controls.Add($txtConfPrep7)

$txtConfPrep6.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 139
$txtConfPrep6.Location = $System_Drawing_Point
$txtConfPrep6.Name = "txtConfPrep6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep6.Size = $System_Drawing_Size
$txtConfPrep6.TabIndex = 15

$grpConfPrep.Controls.Add($txtConfPrep6)

$txtConfPrep5.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 116
$txtConfPrep5.Location = $System_Drawing_Point
$txtConfPrep5.Name = "txtConfPrep5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep5.Size = $System_Drawing_Size
$txtConfPrep5.TabIndex = 14

$grpConfPrep.Controls.Add($txtConfPrep5)

$txtConfPrep4.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 93
$txtConfPrep4.Location = $System_Drawing_Point
$txtConfPrep4.Name = "txtConfPrep4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep4.Size = $System_Drawing_Size
$txtConfPrep4.TabIndex = 13

$grpConfPrep.Controls.Add($txtConfPrep4)

$txtConfPrep3.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 70
$txtConfPrep3.Location = $System_Drawing_Point
$txtConfPrep3.Name = "txtConfPrep3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep3.Size = $System_Drawing_Size
$txtConfPrep3.TabIndex = 12

$grpConfPrep.Controls.Add($txtConfPrep3)

$txtConfPrep2.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 47
$txtConfPrep2.Location = $System_Drawing_Point
$txtConfPrep2.Name = "txtConfPrep2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep2.Size = $System_Drawing_Size
$txtConfPrep2.TabIndex = 11

$grpConfPrep.Controls.Add($txtConfPrep2)

$lblConfPrep9.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 206
$lblConfPrep9.Location = $System_Drawing_Point
$lblConfPrep9.Name = "lblConfPrep9"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep9.Size = $System_Drawing_Size
$lblConfPrep9.TabIndex = 9
$lblConfPrep9.Text = "9"
$lblConfPrep9.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep9)

$lblConfPrep8.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 183
$lblConfPrep8.Location = $System_Drawing_Point
$lblConfPrep8.Name = "lblConfPrep8"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep8.Size = $System_Drawing_Size
$lblConfPrep8.TabIndex = 8
$lblConfPrep8.Text = "8"
$lblConfPrep8.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep8)

$lblConfPrep7.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 160
$lblConfPrep7.Location = $System_Drawing_Point
$lblConfPrep7.Name = "lblConfPrep7"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep7.Size = $System_Drawing_Size
$lblConfPrep7.TabIndex = 7
$lblConfPrep7.Text = "7"
$lblConfPrep7.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep7)

$lblConfPrep6.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 137
$lblConfPrep6.Location = $System_Drawing_Point
$lblConfPrep6.Name = "lblConfPrep6"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep6.Size = $System_Drawing_Size
$lblConfPrep6.TabIndex = 6
$lblConfPrep6.Text = "6"
$lblConfPrep6.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep6)

$lblConfPrep5.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 114
$lblConfPrep5.Location = $System_Drawing_Point
$lblConfPrep5.Name = "lblConfPrep5"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep5.Size = $System_Drawing_Size
$lblConfPrep5.TabIndex = 5
$lblConfPrep5.Text = "5"
$lblConfPrep5.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep5)

$lblConfPrep4.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 91
$lblConfPrep4.Location = $System_Drawing_Point
$lblConfPrep4.Name = "lblConfPrep4"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep4.Size = $System_Drawing_Size
$lblConfPrep4.TabIndex = 4
$lblConfPrep4.Text = "4"
$lblConfPrep4.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep4)

$lblConfPrep3.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 68
$lblConfPrep3.Location = $System_Drawing_Point
$lblConfPrep3.Name = "lblConfPrep3"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep3.Size = $System_Drawing_Size
$lblConfPrep3.TabIndex = 3
$lblConfPrep3.Text = "3"
$lblConfPrep3.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep3)

$lblConfPrep2.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 45
$lblConfPrep2.Location = $System_Drawing_Point
$lblConfPrep2.Name = "lblConfPrep2"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 23
$System_Drawing_Size.Width = 30
$lblConfPrep2.Size = $System_Drawing_Size
$lblConfPrep2.TabIndex = 2
$lblConfPrep2.Text = "2"
$lblConfPrep2.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep2)

$lblConfPrep1.DataBindings.DefaultDataSourceUpdateMode = 0

$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 6
$System_Drawing_Point.Y = 21
$lblConfPrep1.Location = $System_Drawing_Point
$lblConfPrep1.Name = "lblConfPrep1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 24
$System_Drawing_Size.Width = 30
$lblConfPrep1.Size = $System_Drawing_Size
$lblConfPrep1.TabIndex = 1
$lblConfPrep1.Text = "1"
$lblConfPrep1.TextAlign = 32

$grpConfPrep.Controls.Add($lblConfPrep1)

$txtConfPrep1.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 42
$System_Drawing_Point.Y = 24
$txtConfPrep1.Location = $System_Drawing_Point
$txtConfPrep1.Name = "txtConfPrep1"
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Height = 20
$System_Drawing_Size.Width = 152
$txtConfPrep1.Size = $System_Drawing_Size
$txtConfPrep1.TabIndex = 0
$txtConfPrep1.Text = "Make Song Slides"

$grpConfPrep.Controls.Add($txtConfPrep1)

# Add Controls
$grpCountdown.Controls.Add($SyncHash.lblCountdown)
$grpCountdown.Controls.Add($SyncHash.nHour)
$grpCountdown.Controls.Add($SyncHash.nMinute)
$grpCountdown.Controls.Add($SyncHash.nAm)

#endregion Generated Form Code

# On load
LoadTitles
LoadChecklists
LoadConfig

$Global:ReferenceSource = $Global:Config.configuration.obs.selectedSource
write-host $Global:Config
write-host $Global:ReferenceSource -ForegroundColor green


OBSInitialize
OBSConnect
LoadOBSSources


#Save the initial state of the form
$InitialFormWindowState = $formTOC.WindowState
#Init the OnLoad event to correct the initial state of the form
$formTOC.add_Load($OnLoadForm_StateCorrection)
#Show the Form
$formTOC.ShowDialog()| Out-Null

} #End Function

#Call the Function
showTOC
